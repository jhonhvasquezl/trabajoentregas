package co.julianmarin.webContador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebContadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebContadorApplication.class, args);
	}

}
