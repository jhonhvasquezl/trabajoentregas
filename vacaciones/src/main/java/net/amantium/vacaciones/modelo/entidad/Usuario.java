package net.amantium.vacaciones.modelo.entidad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="idusuario")	
	private Integer id;
	
	@JoinColumn(name = "rol_idrol", referencedColumnName = "idrol")
    @ManyToOne(optional = false)	
	private Rol rol_idrol ;
	
	@JoinColumn(name = "idtipo_documento", referencedColumnName = "idtipo_documento")
    @ManyToOne(optional = false)	
	private TipoDocumento idtipo_documento ;

	@Column(name="fecha_nacimiento")	
	private Date fecha_nacimiento;
	
	@JoinColumn(name = "idestado_usuario", referencedColumnName = "idestado_usuario")
    @ManyToOne(optional = false)
	private EstadoUsuario idestado_usuario ;
	
	@Column(name="telefono")	
	private String telefono;
	
	@Column(name="direccion")	
	private String direccion;
	
	@Column(name="numero_documento")	
	private String numero_documento;
	
	@Column(name="login")	
	private String login;
	
	@Column(name="password")	
	private String password;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Rol getRol_idrol() {
		return rol_idrol;
	}

	public void setRol_idrol(Rol rol_idrol) {
		this.rol_idrol = rol_idrol;
	}

	public TipoDocumento getIdtipo_documento() {
		return idtipo_documento;
	}

	public void setIdtipo_documento(TipoDocumento idtipo_documento) {
		this.idtipo_documento = idtipo_documento;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public EstadoUsuario getIdestado_usuario() {
		return idestado_usuario;
	}

	public void setIdestado_usuario(EstadoUsuario idestado_usuario) {
		this.idestado_usuario = idestado_usuario;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNumero_documento() {
		return numero_documento;
	}

	public void setNumero_documento(String numero_documento) {
		this.numero_documento = numero_documento;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

	
	
	
	
	
}
