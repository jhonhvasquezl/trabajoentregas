package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.TipoDocumento;
import net.amantium.vacaciones.modelo.repositorio.ITipoDocumentoRepositorio;

@Service
public class TipoDocumentoService {

	@Autowired
	ITipoDocumentoRepositorio tipoDocumentoRepositorio;
	
	public TipoDocumento getTipoDocumento(Integer id)
	{
		return tipoDocumentoRepositorio.findById(id).orElseThrow();
	}
	
	public List<TipoDocumento> getListTipoDocumento()
	{
		return tipoDocumentoRepositorio.findAll();
	}
	
	public TipoDocumento setGuardarTipoDocumento(TipoDocumento tipoDocumento)
	{
		return tipoDocumentoRepositorio.save(tipoDocumento);
	}
	
	public void setEliminarTipoDocumento(Integer id)
	{
		tipoDocumentoRepositorio.deleteById(id);
	}
}
