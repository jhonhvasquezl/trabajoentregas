package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.Festivo;
import net.amantium.vacaciones.modelo.repositorio.IFestivoRepositorio;

@Service
public class FestivoService {

	@Autowired
	IFestivoRepositorio festivoRepositorio;
	
	public Festivo getEstado(Integer id)
	{
		return festivoRepositorio.findById(id).orElseThrow();
	}
	
	public List<Festivo> getListEstado()
	{
		return festivoRepositorio.findAll();
	}
	
	public Festivo setGuardarEstado(Festivo festivo)
	{
		return festivoRepositorio.save(festivo);
	}
	
	public void setEliminarEstado(Integer id)
	{
		festivoRepositorio.deleteById(id);
	}
}
