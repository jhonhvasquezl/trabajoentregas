package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.EstadoSolicitud;
import net.amantium.vacaciones.modelo.repositorio.IEstadoSolicitudRepositorio;

@Service
public class EstadoSolicitudService {

	@Autowired
	IEstadoSolicitudRepositorio estadoSolicitudRepositorio;
	
	public EstadoSolicitud getEstado(Integer id)
	{
		return estadoSolicitudRepositorio.findById(id).orElseThrow();
	}
	
	public List<EstadoSolicitud> getListEstado()
	{
		return estadoSolicitudRepositorio.findAll();
	}
	
	public EstadoSolicitud setGuardarEstado(EstadoSolicitud estadoSolicitud)
	{
		return estadoSolicitudRepositorio.save(estadoSolicitud);
	}
	
	public void setEliminarEstado(Integer id)
	{
		estadoSolicitudRepositorio.deleteById(id);
	}
}
