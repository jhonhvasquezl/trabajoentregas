package net.amantium.vacaciones.modelo.repositorio;


import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import net.amantium.vacaciones.modelo.entidad.EstadoUsuario;

@Repository
public interface IEstadoUsuarioRepositorio extends JpaRepository<EstadoUsuario, Integer>{


}
