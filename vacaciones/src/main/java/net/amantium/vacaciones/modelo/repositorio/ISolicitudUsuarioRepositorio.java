package net.amantium.vacaciones.modelo.repositorio;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.amantium.vacaciones.modelo.entidad.SolicitudUsuario;

@Repository
public interface ISolicitudUsuarioRepositorio extends JpaRepository<SolicitudUsuario, Integer>{

}
