package net.amantium.vacaciones.modelo.entidad;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "solicitud_usuario")
public class SolicitudUsuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="idsolicitud_usuario")	
	private Integer id;

	@Column(name="idsolicitud_vacaciones")	
	private Integer idsolicitud_vacaciones;
	
	@Column(name="idusuario")	
	private Integer idusuario ;
	
}
