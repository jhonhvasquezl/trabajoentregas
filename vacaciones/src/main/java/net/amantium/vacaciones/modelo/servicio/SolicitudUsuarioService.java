package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.SolicitudUsuario;
import net.amantium.vacaciones.modelo.repositorio.ISolicitudUsuarioRepositorio;

@Service
public class SolicitudUsuarioService {

	@Autowired
	ISolicitudUsuarioRepositorio solicitudUsuarioRepositorio;
	
	public SolicitudUsuario getSolicitudUsuario(Integer id)
	{
		return solicitudUsuarioRepositorio.findById(id).orElseThrow();
	}
	
	public List<SolicitudUsuario> getListSolicitudUsuario()
	{
		return solicitudUsuarioRepositorio.findAll();
	}
	
	public SolicitudUsuario setGuardarSolicitudUsuario(SolicitudUsuario solicitudUsuario)
	{
		return solicitudUsuarioRepositorio.save(solicitudUsuario);
	}
	
	public void setEliminarSolicitudUsuario(Integer id)
	{
		solicitudUsuarioRepositorio.deleteById(id);
	}
}
