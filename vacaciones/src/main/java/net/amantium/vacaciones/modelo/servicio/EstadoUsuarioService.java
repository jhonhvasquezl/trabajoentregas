package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.EstadoUsuario;
import net.amantium.vacaciones.modelo.repositorio.IEstadoUsuarioRepositorio;

@Service
public class EstadoUsuarioService {

	@Autowired
	IEstadoUsuarioRepositorio estadoUsuarioRepositorio;
	
	public EstadoUsuario getEstado(Integer id)
	{
		return estadoUsuarioRepositorio.findById(id).orElseThrow();
	}
	
	public List<EstadoUsuario> getListEstado()
	{
		return estadoUsuarioRepositorio.findAll();
	}
	
	public EstadoUsuario setGuardarEstado(EstadoUsuario estadoUsuario)
	{
		return estadoUsuarioRepositorio.save(estadoUsuario);
	}
	
	public void setEliminarEstado(Integer id)
	{
		estadoUsuarioRepositorio.deleteById(id);
	}
}
