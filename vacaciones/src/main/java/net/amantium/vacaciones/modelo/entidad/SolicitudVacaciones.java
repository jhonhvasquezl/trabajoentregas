package net.amantium.vacaciones.modelo.entidad;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "solicitud_vacaciones")
public class SolicitudVacaciones {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="idsolicitud_vacaciones")	
	private Integer id;
	
	@Column(name="fecha_inicio")	
	private Date fecha_inicio;
	
	@Column(name="fecha_final")	
	private Date fecha_final;
	
	@Column(name="dias")	
	private Integer dias;
	
	@Column(name="razon_solicitud")	
	private String razon_solicitud;

	@JoinColumn(name = "idestado_solicitud", referencedColumnName = "idestado_solicitud")
    @ManyToOne(optional = false)
    private EstadoSolicitud idestado_solicitud;

	@Column(name="verificado_login")	
	private String verificado_login;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	
	public EstadoSolicitud getIdestado_solicitud() {
		return idestado_solicitud;
	}

	public void setIdestado_solicitud(EstadoSolicitud idestado_solicitud) {
		this.idestado_solicitud = idestado_solicitud;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_final() {
		return fecha_final;
	}

	public void setFecha_final(Date fecha_final) {
		this.fecha_final = fecha_final;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public String getRazon_solicitud() {
		return razon_solicitud;
	}

	public void setRazon_solicitud(String razon_solicitud) {
		this.razon_solicitud = razon_solicitud;
	}

	public String getVerificado_login() {
		return verificado_login;
	}

	public void setVerificado_login(String verificado_login) {
		this.verificado_login = verificado_login;
	}
	
	
	
	
	
}
