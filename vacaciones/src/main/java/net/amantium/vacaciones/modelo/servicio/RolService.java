package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.Rol;
import net.amantium.vacaciones.modelo.repositorio.IRolRepositorio;

@Service
public class RolService {

	@Autowired
	IRolRepositorio rolRepositorio;
	
	public Rol getRol(Integer id)
	{
		return rolRepositorio.findById(id).orElseThrow();
	}
	
	public List<Rol> getListRol()
	{
		return rolRepositorio.findAll();
	}
	
	public Rol setGuardarRol(Rol rol)
	{
		return rolRepositorio.save(rol);
	}
	
	public void setEliminarRol(Integer id)
	{
		rolRepositorio.deleteById(id);
	}
}
