package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.SolicitudVacaciones;
import net.amantium.vacaciones.modelo.repositorio.ISolicitudVacacionesRepositorio;

@Service
public class SolicitudVacacionesService {

	@Autowired
	ISolicitudVacacionesRepositorio solicitudVacacionesRepositorio;
	
	public SolicitudVacaciones getEstado(Integer id)
	{
		return solicitudVacacionesRepositorio.findById(id).orElseThrow();
	}
	
	public List<SolicitudVacaciones> getListEstado()
	{
		return solicitudVacacionesRepositorio.findAll();
	}
	
	public SolicitudVacaciones setGuardarEstado(SolicitudVacaciones solicitudVacaciones)
	{
		return solicitudVacacionesRepositorio.save(solicitudVacaciones);
	}
	
	public void setEliminarEstado(Integer id)
	{
		solicitudVacacionesRepositorio.deleteById(id);
	}
}
