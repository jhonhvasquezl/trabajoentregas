package net.amantium.vacaciones.modelo.servicio;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.amantium.vacaciones.modelo.entidad.Usuario;
import net.amantium.vacaciones.modelo.repositorio.IUsuarioRepositorio;

@Service
public class UsuarioService {

	@Autowired
	IUsuarioRepositorio usuarioRepositorio;
	
	public Usuario getUsuario(Integer id)
	{
		return usuarioRepositorio.findById(id).orElseThrow();
	}
	
	public List<Usuario> getListUsuario()
	{
		return usuarioRepositorio.findAll();
	}
	
	public Usuario setGuardarUsuario(Usuario usuario)
	{
		return usuarioRepositorio.save(usuario);
	}
	
	public void setEliminarUsuario(Integer id)
	{
		usuarioRepositorio.deleteById(id);
	}
}
