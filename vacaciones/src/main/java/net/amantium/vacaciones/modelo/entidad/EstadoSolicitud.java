package net.amantium.vacaciones.modelo.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estado_solicitud")
public class EstadoSolicitud {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="idestado_solicitud")	
	private Integer id;
	
	@Column(name="nombre")	
	private String 	nombre;
	
	

	public EstadoSolicitud(Integer id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	
	public EstadoSolicitud() {
		// TODO Auto-generated constructor stub
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


}
