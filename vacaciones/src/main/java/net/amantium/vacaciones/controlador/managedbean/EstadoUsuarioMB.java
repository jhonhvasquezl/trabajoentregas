package net.amantium.vacaciones.controlador.managedbean;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.EstadoUsuario;
import net.amantium.vacaciones.modelo.servicio.EstadoUsuarioService;

@Named
@ViewScoped
public class EstadoUsuarioMB implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private EstadoUsuarioService estadoUsuarioService;
	
	private EstadoUsuario estado;

	private Map<String, Integer> estadosCombo = new HashMap<>();

	

	@PostConstruct
	public void init() {
		setEstado(new EstadoUsuario());
		estadosCombo = new HashMap<>();
		for (EstadoUsuario es : estadoUsuarioService.getListEstado()) {
			estadosCombo.put(es.getNombre(),es.getId());
		}
	}

	
	
	public Map<String, Integer> getEstadosCombo() {
		return estadosCombo;
	}



	public void setEstadosCombo(Map<String, Integer> estadosCombo) {
		this.estadosCombo = estadosCombo;
	}



	public EstadoUsuario getEstado() {
		return estado;
	}

	public void setEstado(EstadoUsuario estado) {
		this.estado = estado;
	}

	public List<EstadoUsuario> getListEstado() {
		return estadoUsuarioService.getListEstado();
	}
	
	public String guardar()
	{
		
		estadoUsuarioService.setGuardarEstado(getEstado());
		setEstado(new EstadoUsuario());
		return "/estado-usuario/listEstadoUsuario.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id)
	{
        setEstado(estadoUsuarioService.getEstado(id));
		return "/estado-usuario/editEstadoUsuario.xhtml?faces-redirect=true";
	}
	public String actualizar()
	{
		estadoUsuarioService.setGuardarEstado(getEstado());
		setEstado(new EstadoUsuario());
		return "/estado-usuario/listEstadoUsuario.xhtml?faces-redirect=true";
	}
	
	public String elminimar(Integer id)
	{
		estadoUsuarioService.setEliminarEstado(id);
		return "/index.xhtml?faces-redirect=true";
	}

	
}
