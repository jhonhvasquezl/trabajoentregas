package net.amantium.vacaciones.controlador.managedbean;


import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.SolicitudUsuario;
import net.amantium.vacaciones.modelo.servicio.SolicitudUsuarioService;

@Named
@ViewScoped
public class SolicitudUsuarioMB implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private SolicitudUsuarioService solicitudUsuarioService;
	
	private SolicitudUsuario solicitudUsuario;
	

	@PostConstruct
	public void init() {
		setSolicitudUsuario(new SolicitudUsuario());
	}

	public SolicitudUsuario getSolicitudUsuario() {
		return solicitudUsuario;
	}


	public void setSolicitudUsuario(SolicitudUsuario solicitudusuario) {
		this.solicitudUsuario = solicitudusuario;
	}





	public List<SolicitudUsuario> getListSolicitudUsuario() {
		return solicitudUsuarioService.getListSolicitudUsuario();
	}
	
	public void onDateSelect() {
		System.out.println("si entra");
	}
	
	public String guardar()
	{
		
		solicitudUsuarioService.setGuardarSolicitudUsuario(getSolicitudUsuario());
		setSolicitudUsuario(new SolicitudUsuario());
		return "/solicitud-usuario/listFestivo.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id)
	{
		setSolicitudUsuario(solicitudUsuarioService.getSolicitudUsuario(id));
		return "/solicitud-usuario/editFestivo.xhtml?faces-redirect=true";
	}
	public String actualizar()
	{
		solicitudUsuarioService.setGuardarSolicitudUsuario(getSolicitudUsuario());
		return "/solicitud-usuario/listFestivo.xhtml?faces-redirect=true";
	}
	
	public String elminimar(Integer id)
	{
		solicitudUsuarioService.setEliminarSolicitudUsuario(id);
		return "/solicitud-usuario/index.xhtml?faces-redirect=true";
	}

	
}
