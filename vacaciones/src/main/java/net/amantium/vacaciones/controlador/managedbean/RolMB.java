package net.amantium.vacaciones.controlador.managedbean;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.Rol;
import net.amantium.vacaciones.modelo.servicio.RolService;

@Named
@ViewScoped
public class RolMB implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private RolService rolService;
	
	private Rol rol;

	private Map<String, Integer> estadosCombo = new HashMap<>();


	@PostConstruct
	public void init() {
		setRol(new Rol());
		for (Rol es : rolService.getListRol()) {
			estadosCombo.put(es.getNombre(),es.getId());
		}
	}

	
	
	public Map<String, Integer> getEstadosCombo() {
		return estadosCombo;
	}



	public void setEstadosCombo(Map<String, Integer> estadosCombo) {
		this.estadosCombo = estadosCombo;
	}



	public Rol getRol() {
		return rol;
	}


	public void setRol(Rol rol) {
		this.rol = rol;
	}





	public List<Rol> getListRol() {
		return rolService.getListRol();
	}
	
	public String guardar()
	{
		
		rolService.setGuardarRol(getRol());
		setRol(new Rol());
		return "/rol/listRol.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id)
	{
		setRol(rolService.getRol(id));
		return "/rol/editRol.xhtml?faces-redirect=true";
	}
	public String actualizar()
	{
		rolService.setGuardarRol(getRol());
		return "/rol/listRol.xhtml?faces-redirect=true";
	}
	
	public String elminimar(Integer id)
	{
		rolService.setEliminarRol(id);
		return "/rol/listRol.xhtml?faces-redirect=true";
	}

	
}
