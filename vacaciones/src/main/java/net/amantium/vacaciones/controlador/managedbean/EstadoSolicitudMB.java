package net.amantium.vacaciones.controlador.managedbean;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.EstadoSolicitud;
import net.amantium.vacaciones.modelo.servicio.EstadoSolicitudService;

@Named
@ViewScoped
public class EstadoSolicitudMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private EstadoSolicitudService estadoSolicitudService;

	private EstadoSolicitud estado;

	private Map<String, Integer> estadosCombo = new HashMap<>();

	@PostConstruct
	public void init() {
		setEstado(new EstadoSolicitud());
		estadosCombo = new HashMap<>();
		for (EstadoSolicitud es : estadoSolicitudService.getListEstado()) {
			estadosCombo.put(es.getNombre(),es.getId());
		}

	}

	public Map<String, Integer> getEstadosCombo() {
		return estadosCombo;
	}

	public void setEstadosCombo(Map<String, Integer> estadosCombo) {
		this.estadosCombo = estadosCombo;
	}

	public EstadoSolicitud getEstado() {
		return estado;
	}

	public void setEstado(EstadoSolicitud estado) {
		this.estado = estado;
	}

	public List<EstadoSolicitud> getListEstado() {
		return estadoSolicitudService.getListEstado();
	}



	public String guardar() {

		estadoSolicitudService.setGuardarEstado(getEstado());
		setEstado(new EstadoSolicitud());
		return "/estado-solicitud/listEstadoSolicitud.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id) {
		setEstado(estadoSolicitudService.getEstado(id));
		return "/estado-solicitud/editEstadoSolicitud.xhtml?faces-redirect=true";
	}

	public String actualizar() {
		estadoSolicitudService.setGuardarEstado(getEstado());
		setEstado(new EstadoSolicitud());
		return "/estado-solicitud/listEstadoSolicitud.xhtml?faces-redirect=true";
	}

	public String elminimar(Integer id) {
		estadoSolicitudService.setEliminarEstado(id);
		return "/estado-solicitud/index.xhtml?faces-redirect=true";
	}

}
