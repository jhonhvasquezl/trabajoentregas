package net.amantium.vacaciones.controlador.managedbean;

import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.EstadoSolicitud;
import net.amantium.vacaciones.modelo.entidad.SolicitudVacaciones;
import net.amantium.vacaciones.modelo.servicio.SolicitudVacacionesService;

@Named
@ViewScoped
public class SolicitudVacacionesMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private SolicitudVacacionesService solicitudVacacionesService;

	private SolicitudVacaciones solicitud;
	private List<Date> rango;
	private ScheduleModel eventModel;

	@PostConstruct
	public void init() {
		setSolicitud(new SolicitudVacaciones());
		getSolicitud().setIdestado_solicitud(new EstadoSolicitud());
		eventModel = new DefaultScheduleModel();
		asignarCalendario();

	}

	


	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public List<Date> getRango() {
		return rango;
	}

	public void setRango(List<Date> rango) {
		this.rango = rango;
	}

	public SolicitudVacaciones getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudVacaciones solicitud) {
		this.solicitud = solicitud;
	}

	public List<SolicitudVacaciones> getListSolicitudVacaciones() {
		return solicitudVacacionesService.getListEstado();
	}

	void asignarCalendario() {

		for (SolicitudVacaciones sol : getListSolicitudVacaciones()) {

	        Random random = new Random();
	        int nextInt = random.nextInt(0xffffff + 1);
	        String colorCode = String.format("#%06x", nextInt);
			DefaultScheduleEvent<?> event = DefaultScheduleEvent.builder()
					.title("Vacaciones Nro" + sol.getId())
					.startDate(LocalDateTime.ofInstant(sol.getFecha_inicio().toInstant(), ZoneId.systemDefault()))
					.endDate(LocalDateTime.ofInstant(sol.getFecha_final().toInstant(), ZoneId.systemDefault()))
					.description(sol.getRazon_solicitud())
					.backgroundColor(colorCode)
					.build();
			eventModel.addEvent(event);
		}

	}

	public void tratamientoFecha() {
		solicitud.setFecha_inicio(getRango().get(0));
		solicitud.setFecha_final(getRango().get(1));
		Long diferencia = solicitud.getFecha_final().getTime() - solicitud.getFecha_inicio().getTime();
		Integer dias = (int) (diferencia / (1000 * 60 * 60 * 24));
		solicitud.setDias(dias);
	}

	public String guardar() {
		// tratamientoFecha();
		// EstadoSolicitud estado= new EstadoSolicitud();
		// estado.setId(2);
		// solicitud.setIdestado_solicitud(estado);
		solicitudVacacionesService.setGuardarEstado(getSolicitud());
		setSolicitud(new SolicitudVacaciones());
		return "/solicitud-vacaciones/listSolicitudVacaciones.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id) {
		setSolicitud(solicitudVacacionesService.getEstado(id));
		return "/solicitud-vacaciones/editSolicitudVacaciones.xhtml?faces-redirect=true";
	}

	public String actualizar() {
		solicitudVacacionesService.setGuardarEstado(getSolicitud());
		setSolicitud(new SolicitudVacaciones());
		return "/solicitud-vacaciones/listSolicitudVacaciones.xhtml?faces-redirect=true";
	}

	public String elminimar(Integer id) {
		solicitudVacacionesService.setEliminarEstado(id);
		return "/solicitud-vacaciones/index.xhtml?faces-redirect=true";
	}

}
