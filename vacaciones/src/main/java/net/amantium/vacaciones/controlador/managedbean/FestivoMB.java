package net.amantium.vacaciones.controlador.managedbean;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.Festivo;
import net.amantium.vacaciones.modelo.servicio.FestivoService;

@Named
@ViewScoped
public class FestivoMB implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private FestivoService festivoService;
	
	private Festivo festivo;
	

	@PostConstruct
	public void init() {
		setFestivo(new Festivo());
	}

	public Festivo getFestivo() {
		return festivo;
	}


	public void setFestivo(Festivo festivo) {
		this.festivo = festivo;
	}





	public List<Festivo> getListEstado() {
		return festivoService.getListEstado();
	}
	
	public String guardar()
	{
		
		festivoService.setGuardarEstado(getFestivo());
		setFestivo(new Festivo());
		return "/festivo/listFestivo.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id)
	{
		setFestivo(festivoService.getEstado(id));
		return "/festivo/editFestivo.xhtml?faces-redirect=true";
	}
	public String actualizar()
	{
		festivoService.setGuardarEstado(getFestivo());
		setFestivo (new Festivo());
		return "/festivo/listFestivo.xhtml?faces-redirect=true";
	}
	
	public String elminimar(Integer id)
	{
		festivoService.setEliminarEstado(id);
		return "/festivo/index.xhtml?faces-redirect=true";
	}

	
}
