package net.amantium.vacaciones.controlador.managedbean;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.EstadoUsuario;
import net.amantium.vacaciones.modelo.entidad.Rol;
import net.amantium.vacaciones.modelo.entidad.TipoDocumento;
import net.amantium.vacaciones.modelo.entidad.Usuario;
import net.amantium.vacaciones.modelo.servicio.UsuarioService;

@Named
@ViewScoped
public class UsuarioMB implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private UsuarioService usuarioService;
	
	private Usuario usuario;
	

	@PostConstruct
	public void init() {
		setUsuario(new Usuario());
		getUsuario().setIdestado_usuario(new EstadoUsuario());
		getUsuario().setIdtipo_documento(new TipoDocumento());
		getUsuario().setRol_idrol(new Rol());
	}

	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}





	public List<Usuario> getListUsuario() {
		return usuarioService.getListUsuario();
	}
	
	public String guardar()
	{
		
		usuarioService.setGuardarUsuario(getUsuario());
		setUsuario(new Usuario());
		return "/usuario/listUsuario.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id)
	{
		setUsuario(usuarioService.getUsuario(id));
		return "/usuario/editUsuario.xhtml?faces-redirect=true";
	}
	public String actualizar()
	{
		usuarioService.setGuardarUsuario(getUsuario());
		return "/usuario/listUsuario.xhtml?faces-redirect=true";
	}
	
	public String elminimar(Integer id)
	{
		usuarioService.setEliminarUsuario(id);
		return "/usuario/listUsuario.xhtml?faces-redirect=true";
	}

	
}
