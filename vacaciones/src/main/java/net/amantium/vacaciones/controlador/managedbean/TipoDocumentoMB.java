package net.amantium.vacaciones.controlador.managedbean;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import net.amantium.vacaciones.modelo.entidad.TipoDocumento;
import net.amantium.vacaciones.modelo.servicio.TipoDocumentoService;

@Named
@ViewScoped
public class TipoDocumentoMB implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	
	private TipoDocumento tipoDocumento;
	
	private Map<String, Integer> estadosCombo = new HashMap<>();


	@PostConstruct
	public void init() {
		setTipoDocumento(new TipoDocumento());
		for (TipoDocumento es : tipoDocumentoService.getListTipoDocumento()) {
			estadosCombo.put(es.getNombre(),es.getId());
		}
	}

	
	
	public Map<String, Integer> getEstadosCombo() {
		return estadosCombo;
	}



	public void setEstadosCombo(Map<String, Integer> estadosCombo) {
		this.estadosCombo = estadosCombo;
	}



	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(TipoDocumento tipodocumento) {
		this.tipoDocumento = tipodocumento;
	}





	public List<TipoDocumento> getListTipoDocumento() {
		return tipoDocumentoService.getListTipoDocumento();
	}
	
	public String guardar()
	{
		
		tipoDocumentoService.setGuardarTipoDocumento(getTipoDocumento());
		setTipoDocumento(new TipoDocumento());
		return "/tipo-documento/listTipoDocumento.xhtml?faces-redirect=true";
	}

	public String preparoActualizar(Integer id)
	{
		setTipoDocumento(tipoDocumentoService.getTipoDocumento(id));
		return "/tipo-documento/editTipoDocumento.xhtml?faces-redirect=true";
	}
	public String actualizar()
	{
		tipoDocumentoService.setGuardarTipoDocumento(getTipoDocumento());
		return "/tipo-documento/listTipoDocumento.xhtml?faces-redirect=true";
	}
	
	public String elminimar(Integer id)
	{
		tipoDocumentoService.setEliminarTipoDocumento(id);
		return "/tipo-documento/listTipoDocumento.xhtml?faces-redirect=true";
	}

	
}
