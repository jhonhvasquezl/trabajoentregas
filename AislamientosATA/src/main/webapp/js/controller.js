angular.module('app',[]).controller('ContactController', [ '$scope','$http','$window',function(s,h,w) {
	s.form={};
	s.exp= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	s.submitbusqueda= function() {
		if (s.formulario.$valid) 
		{
			$('#loader').show();
			h.get('https://pleionecenter.appspot.com/_ah/api/comentariobacking/v1/agregarComentarioWeb/'+s.form.usuNombre+'/'+s.form.usuApellido+'/'+s.form.usuCorreo+'/'+s.form.usuCelular+'//'+s.form.usuMensaje+'/'+222222).then(function (data){
				if(data.data!='')
				{
					$('#loader').hide();
					s.form={};
	                w.alert("Inscripción realizada de manera exitosa, pronto nos estaremos comunicando contigo");
				}
				else
				{
					$('#loader').hide();
					w.alert("No pudimos procesar tu solicitud, inténtalo nuevamente");

				}
			   },function (error){
				   $('#loader').hide();
					w.alert("Error en la conexión, inténtalo nuevamente mas tarde");
			   });	
			
		}
		    
	}
}]);