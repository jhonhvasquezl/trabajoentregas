<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all">
<!--tages-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Sintony:400,700'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Fjalla+One'
	rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>




<%@include file="/template/header.html"%>
</head>
<body>
	
	<div class="content">
		<div class="container">
			<!--start-image-gallery-->
			<div class="gallerys">
				<h3>GALER�A</h3>
				<h5></h5>
				<div class="clearfix"></div>
				
				<div class="gallery-grids">
					
					<div id="paginationdemo" class="demo">
		  
				<div id="p1" class="gallery-grids pagedemo _current" >
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/1.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/1.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid grid2">
						<a href="images/ImagenesATA/2.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"> <img src="images/ImagenesATA/2.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/3.jpg" class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox" title="Image Title">
							<img src="images/ImagenesATA/3.jpg" class="img-responsive" alt="">
						</a>
						
						
					</div>
					
					
					<div class="clearfix"></div>
				</div>
			
				<div id="p2" class="gallery-grids pagedemo" style="display: none;">
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/4.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/4.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid grid2">
						<a href="images/ImagenesATA/5.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"> <img src="images/ImagenesATA/5.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/6.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/6.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div id="p3" class="gallery-grids pagedemo" style="display: none;">
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/7.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/7.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid grid2">
						<a href="images/ImagenesATA/8.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"> <img src="images/ImagenesATA/8.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/9.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/9.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="clearfix"></div>
				</div>
				
				
				<div id="p4" class="gallery-grids pagedemo" style="display: none;">
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/10.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/10.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid grid2">
						<a href="images/ImagenesATA/11.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"> <img src="images/ImagenesATA/11.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/12.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/12.jpg"
							class="img-responsive" alt=""></a>
						
						
						
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div id="p5" class="gallery-grids pagedemo" style="display: none;">
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/13.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/13.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					<div class="col-md-4 gallery-grid grid2">
						<a href="images/ImagenesATA/14.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"> <img src="images/ImagenesATA/14.jpg"
							class="img-responsive" alt=""></a>
						
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/15.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/15.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div id="p6" class="gallery-grids pagedemo" style="display: none;">
					
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/17.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/17.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/16.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/16.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/22.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/22.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					
					<div class="clearfix"></div>
				</div>
				
				<div id="p7" class="gallery-grids pagedemo" style="display: none;">
					
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/19.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/19.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/20.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/20.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/21.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/21.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					
					<div class="clearfix"></div>
				</div>
				
				<div id="p8" class="gallery-grids pagedemo" style="display: none;">
					
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/23.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/23.jpg"
							class="img-responsive" alt=""></a>
						
						
					</div>
					<div class="col-md-4 gallery-grid">
						<a href="images/ImagenesATA/24.jpg"
							class="b-link-stripe b-animate-go  swipebox  img-responsive"
							title="Image Title" class="b-link-stripe b-animate-go  swipebox"
							title="Image Title"><img src="images/ImagenesATA/24.jpg"
							class="img-responsive" alt=""></a>
						
					</div>
					
					
					<div class="clearfix"></div>
				</div>
				
				<div id="demo5">
                </div>		
			
			</div>
			<!--End-image-gallery-->
			<div class="clearfix"></div>
		</div>
	</div>
	<!--swipebox-->
	<link rel="stylesheet" href="css/swipebox.css">
	<script src="js/jquery.swipebox.min.js"></script>
	<script type="text/javascript">
		jQuery(function($) {
			$(".swipebox").swipebox();
		});

		$(function () {            
            $("#demo5").paginate({
                count: 8,
                start: 1,
                display: 5,
                border: true,
                border_color: '#fff',
                text_color: '#fff',
                background_color: '#3B05A2',
                border_hover_color: '#ccc',
                text_hover_color: '#000',
                background_hover_color: '#fff',
                images: false,
                mouse: 'press',
                onChange: function (page) {
                    $('._current', '#paginationdemo').removeClass('_current').hide();
                    $('#p' + page).addClass('_current').show();
                }
            });
        });
	</script>
	</div>
	</div>
	<%@include file="/template/footer.html"%>	
</body>
<link href="/css/stylepag.css" rel="stylesheet" type="text/css"/>
<script src="js/jquery-1.3.2.js"></script>
<script src="js/jquery.paginate.js"></script>
</html>
