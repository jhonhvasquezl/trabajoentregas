<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all">
<!--tages-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Sintony:400,700'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Fjalla+One'
	rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<%@include file="/template/header.html"%>
	<div class="content">
		<div class="container">
			<!--start-about-->
			<div class="about">
				<div class="about-grids">
					<div class="col-md-4 about-grid">
						<h3>�Quienes somos?</h3>
						<h5></h5>
						<div class="clearfix"></div>
						<a href="#"><img src="images/slider1.jpg"
							class="img-responsive" alt=""></a> <span>
							<p align="justify">
							Somos una
							empresa con m�s de 30 a�os de experiencia en aislamientos
							t�rmicos, ac�sticos y montajes de los mismos en la industria
							nacional, nuestros valores corporativos:
							</p>
							</span>
						<p>
							Integridad <br> Responsabilidad <br> Respeto <br>
							Excelencia <br> Actitud positiva <br>
						</p>
					</div>
					<div class="col-md-4 about-grid center-grid1">
						<h3>�Hacia donde vamos?</h3>
						<h5></h5>
						<div class="clearfix"></div>
						<label>MISI�N</label>
						<p align="justify">Somos una empresa dedicada a la comercializaci�n de productos t�rmicos y ac�sticos para lograr ambientes c�modos y seguros en la industria y la construcci�n, caracteriz�ndonos por la calidad, disponibilidad inmediata de productos y prontitud de respuesta en todos los procesos.</p>
						<label>VISI�N</label>
						<p align="justify">Posicionarnos en Colombia como una Empresa l�der en el montaje y mantenimiento de Aislamientos T�rmicos, ofreciendo calidad a nuestros clientes y generando para ellos nuevas alternativas que de manera eficaz mejoren sus procesos t�rmicos. <br><br> Fortalecer la relaci�n comercial con nuestros clientes, obtener mayor participaci�n en el mercado y ampliar la cobertura a diferentes regiones del pa�s.</p>
					</div>
					<div class="col-md-4 about-grid last-grid">
						<h3>�Que hacemos?</h3>
						<h5></h5>
						<div class="clearfix"></div>
						<div class="about-team">
							<div class="client">
								<div class="about-team-left">
									<a href="#"><img src="images/100x100/20.jpg" class="img-responsive"
										alt=""></a>
								</div>
								<div class="about-team-right">
									<p align="justify">L�minas elaboradas con espuma r�gida de poliuretano. Disponible con recubrimiento de papel foil o papel kraft. Dise�adas para el aislamiento de sistemas industriales (cuartos fr�os o pasteurizadoras) que operan a bajas temperaturas desde -15 �C hasta 60 �C.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#"><img src="images/100x100/18.jpg" class="img-responsive"
										alt=""></a>
								</div>
								<div class="about-team-right">
									<p align="justify">Ca�uelas de poliuretano que est�n dise�ados apara el aislamiento termino de procesos, equipos y tuber�a en la industria del fr�o y la refrigeraci�n.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#"><img src="images/100x100/13.jpg" class="img-responsive"
										alt=""></a>
								</div>
								<div class="about-team-right">
									<p align="justify">Sistemas ac�sticos para oficinas, auditorios, estudios de grabaci�n, cuartos de m�sica y tratamiento de equipos industriales, como plantas el�ctricas, sopladores, compresores y bombas.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="client">
								<div class="about-team-left">
									<a href="#"><img src="images/100x100/p2.jpg" class="img-responsive"
										alt=""></a>
								</div>
								<div class="about-team-right">
									<p align="justify">Montaje de sistemas constructivos con la normatividad Invima para la industria de alimentos. Adem�s pueden integrarse a sistemas de iluminaci�n y ventilaci�n natural o forzada.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!--End-about-->
			<div class="clearfix"></div>
<div class="recent-places">
				<h4>Nuestro trabajo</h4>
				<h5></h5>
				<div class="clearfix"></div>
				<div class="col-md-3 holder smooth">
					<img src="images/300X200/15.jpg" alt="Web Tutorial Plus - Demo">
					<div class="go-top">
						<h3>Responsabilidad</h3>
						<p align="justify">Tenemos la habilidad para responder por los compromisos asignados por la Empresa con motivaci�n, decisi�n y disciplina, asumiendo las consecuencias por las acciones o por las omisiones de esos actos.</p>
						
					</div>
				</div>
				<div class="col-md-3 holder smooth">
					<img src="images/300X200/4.jpg" alt="Web Tutorial Plus - Demo">
					<div class="go-top">
						<h3>Integridad</h3>
						<p align="justify">Actuamos coherentemente con firmeza, honestidad y sinceridad obrando siempre bien, sin que tengan que vigilar nuestro comportamiento.</p>
					</div>
				</div>
				<div class="col-md-3 holder smooth">
					<img src="images/300X200/8.jpg" alt="Web Tutorial Plus - Demo">
					<div class="go-top">
						<h3>Respeto</h3>
						<p align="justify">Desarrollamos la sensibilidad para reconocer y valorar a las personas, el medio ambiente y los bienes de la organizaci�n propiciando armon�a en las relaciones interpersonales, laborales y comerciales.</p>
					</div>
				</div>
				<div class="col-md-3 holder smooth last-grid">
					<img src="images/300X200/p1.jpg" alt="Web Tutorial Plus - Demo">
					<div class="go-top">
						<h3>Excelencia</h3>
						<p align="justify">Somos personas que se esfuerzan por cumplir con sus responsabilidades con calidad excepcional, buscando el mejoramiento continuo y la m�xima innovaci�n, para conseguir resultados �ptimos.</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<%@include file="/template/footer.html"%>
</body>
</html>

