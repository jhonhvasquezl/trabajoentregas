<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all">
<!--tages-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript">
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 


</script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Sintony:400,700'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Fjalla+One'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/responsiveslides.css">
<script src="js/jquery.min.js"></script>
<script src="js/responsiveslides.min.js"></script>


<script>
	// You can also use "$(window).load(function() {"
	$(function() {
		// Slideshow 1
		$("#slider1").responsiveSlides({
			maxwidth : 1600,
			speed : 600
		});
	});
</script>
<script src="js/bootstrap.min.js"></script>
</head>
<body>
	<%@include file="/template/header.html"%>

	<div class="image-slider">
		<!-- Slideshow 1 -->
		<ul class="rslides" id="slider1" >
			<li><img src="images/slider2.jpg" alt=""></li>
			<li><img src="images/slider3.jpg" alt=""></li>
			<li><img src="images/slider4.jpg" alt=""></li>
			<li><img src="images/slider5.jpg" alt=""></li>
			<li><img src="images/slider1.jpg" alt=""></li>

		</ul>
		<!-- Slideshow 2 -->
	</div>
	<div class="content">
		<div class="container">
			<div class="grids">
				<h4>�Que Aislamientos Hacemos?</h4>
				<h5></h5>
				<div class="clearfix"></div>
				<div class="section group" >
				
					<div class="col-md-3 we-do">
						<div class="active-grid">
							<h3>
								<img src="images/g4.png" title="support" alt=""> T�rmicos
							</h3>
							<p>Realizamos aislamientos industriales para procesos en
								caliente y fri�, garantizando mayor eficiencia t�rmica y ahorro
								de energ�a.</p>
						</div>
					</div>
					<div class="col-md-3 we-do">
						<div class="active-grid">
							<h3>
								<img src="images/g1.png" title="Destinations" alt="">AC�STICOS
							</h3>
							<p>Sistemas ac�sticos para absorber el sonido en diferentes
								frecuencias y reducir la transmisi�n de ruido, generando as�
								mayor privacidad, claridad en el sonido y confort</p>
						</div>
					</div>
					<div class="col-md-3 we-do">
						<div class="active-grid">
							<h3>
								<img src="images/g2.png" title="Events" alt="">Constructivos
							</h3>
							<p>Montaje de sistemas constructivos Metecno en fachadas,
								divisiones, cubiertas, cuartos fr�os y puertas para la industria
								de alto rendimiento</p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="big-button">
			<div class="big-b-text">
				<p>Nos caracteriza</p>
				<span>La calidad, disponibilidad inmediata de productos y
					prontitud de respuesta en todos los procesos...</span>
			</div>
			<div class="big-b-btn">
				<a href="../about.jsp">Leer m�s</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="recent-places">
			<h4>Nuestro trabajo</h4>
			<h5></h5>
			<div class="clearfix"></div>
			<div class="col-md-3 holder smooth">
				<img src="images/300X200/15.jpg" alt="Web Tutorial Plus - Demo">
				<div class="go-top">
					<h3>Responsabilidad</h3>
					<p>Tenemos la habilidad para responder por los compromisos
						asignados por la Empresa con motivaci�n, decisi�n y disciplina,
						asumiendo las consecuencias por las acciones o por las omisiones
						de esos actos.</p>

				</div>
			</div>
			<div class="col-md-3 holder smooth">
				<img src="images/300X200/4.jpg" alt="Web Tutorial Plus - Demo">
				<div class="go-top">
					<h3>Integridad</h3>
					<p>Actuamos coherentemente con firmeza, honestidad y sinceridad
						obrando siempre bien, sin que tengan que vigilar nuestro
						comportamiento.</p>
				</div>
			</div>
			<div class="col-md-3 holder smooth">
				<img src="images/300X200/8.jpg" alt="Web Tutorial Plus - Demo">
				<div class="go-top">
					<h3>Respeto</h3>
					<p>Desarrollamos la sensibilidad para reconocer y valorar a las
						personas, el medio ambiente y los bienes de la organizaci�n
						propiciando armon�a en las relaciones interpersonales, laborales y
						comerciales.</p>
				</div>
			</div>
			<div class="col-md-3 holder smooth last-grid">
				<img src="images/300X200/p1.jpg" alt="Web Tutorial Plus - Demo">
				<div class="go-top">
					<h3>Excelencia</h3>
					<p>Somos personas que se esfuerzan por cumplir con sus
						responsabilidades con calidad excepcional, buscando el
						mejoramiento continuo y la m�xima innovaci�n, para conseguir
						resultados �ptimos.</p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<%@include file="/template/footer.html"%>
</body>

</html>


