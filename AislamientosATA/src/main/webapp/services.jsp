<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all">
<!--tages-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Sintony:400,700'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Fjalla+One'
	rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<link href="/css/stylepag.css" rel="stylesheet" type="text/css" />


</head>
<body>
	<%@include file="/template/header.html"%>







	<div class="content">
		<div class="container">
			<!--start-services-->
			<div class="services">
				<h4>CA�UELAS Y L�MINAS DE POLIURETANO</h4>
				<h5></h5>


				<div class="accordionWrapper">
					<div class="accordionItem open">
						<h2 class="accordionItemHeading">Descripci�n</h2>
						<div class="accordionItemContent">
							<table style="width: 100%">
								<tr>
									<td style="width: 70%; padding: 30px;">
										<p ALIGN="justify">Las ca�uelas y l�minas de poliuretano
											auto-extinguibles son fabricadas con espuma de poliuretano
											con una densidad media de 40kg/m� presentadas en forma de
											ca�uelas y laminas planas que se caracterizan por el bajo
											factor de conductividad t�rmica (K), rigidez, peso liviano y
											resistencia mec�nica.</p>
										<p ALIGN="justify">La estructura de celdas cerradas
											contribuyen a rechazar las humedad en los procesos fr�os y la
											refrigeraci�n.</p>
									</td>
									<td style="width: 30%"><img
										src="images/ImagenesATA/18.jpg" class="img-responsive" alt="" />


									</td>
								</tr>
							</table>

						</div>
					</div>

					<div class="accordionItem close">
						<h2 class="accordionItemHeading">Usos</h2>
						<div class="accordionItemContent">
							<table style="width: 100%">
								<tr>
									<td style="width: 50%; padding: 30px;">
										<p ALIGN="justify">Las ca�uelas y laminas de poliuretano
											est�n dise�adas para el aislamiento t�rmico de procesos,
											equipos y tuber�a en la industria del frio y la
											refrigeraci�n.</p>

									</td>
									<td style="width: 50%"><img src="images/p3.jpg" alt="" />


									</td>

								</tr>
								<tr>

								</tr>
							</table>
						</div>
					</div>

					<div class="accordionItem close">
						<h2 class="accordionItemHeading">Ventajas</h2>
						<div class="accordionItemContent">
							<table style="width: 100%">
								<tr>
									<td style="width: 55%; padding: 30px;">
										<h3>Eficiencia t�rmica</h3>
										<p ALIGN="justify">Poseen un bajo factor de conductividad
											t�rmica (K) reduciendo dr�sticamente las ganancias de calor.</p>

										<h3>Estabilidad dimensional</h3>
										<p ALIGN="justify">Su estructura de celdas cerradas le
											proporciona rigidez y estabilidad. Se adaptan perfectamente a
											los tubos y pueden estar sometidas a trabajo intenso y
											continuo.</p>

										<h3>Resistencia a la compresi�n</h3>
										<p ALIGN="justify">Presenta una excelente resistencia a la
											compresi�n, que permite su uso, manipulaci�n e instalaci�n
											sin ning�n riesgo de perder sus propiedades y estabilidad.
											Presenta una resistencia nominal a la compresi�n de 40psi.</p>

										<h3>Auto extinguible</h3>
										<p ALIGN="justify">El poliuretano utilizado en la
											manufactura de las ca�uelas posee un aditivo retardante
											contra el fuego y esta clasificado como auto extinguible
											seg�n la norma ASTM D 1692.</p>

										<h3>Resistencia a la humedad</h3>
										<p ALIGN="justify">Su estructura de celda cerrada presenta
											una gran resistencia a la absorci�n y muy baja permeabilidad
											a la humedad.</p>

										<h3>F�cil instalaci�n</h3>
										<p ALIGN="justify">Son f�ciles de instalar, sin livianas y
											por su longitud (1ml) la instalaci�n rinde mucho mas,
											disminuyendo costos de mano de obra.</p>

										<h3>Densidad uniforme</h3>
										<p ALIGN="justify">Su proceso de fabricaci�n permite que
											las ca�uelas mantengan la densidad uniforme en toda la
											longitud, conservando el factor de conductividad t�rmica
											invariable.</p>

										<h3>Excelente presentaci�n</h3>
										<p ALIGN="justify">Viene en dos presentaciones sin foil de
											aluminio para recubrir con lamina de aluminio y con foil de
											aluminio reforzado para tuber�a. No requiere acabado final.</p>

									</td>
									<td style="width: 45%"><img
										src="images/ImagenesATA/21.jpg" class="img-responsive" alt="" />


									</td>
								</tr>
							</table>
						</div>
					</div>

					<div class="accordionItem close">
						<h2 class="accordionItemHeading">Propiedades</h2>
						<div class="accordionItemContent">
							<table style="width: 100%">
								<tr>
									<td style="width: 70%; padding: 30px;">
										<h2>Ca�uelas</h2>
										<p ALIGN="justify">Las ca�uelas salen al mercado en
											di�metros dimensionales desde 1/2 hasta 12 pulgadas y
											espesores de aislamiento de 1 a 2" pulgadas.</p>
										<p ALIGN="justify">Disponibles sin recubrimiento , con
											barrera de vapor en foil de aluminio o papel kraft.</p>

										<h2>Laminas Planas</h2>
										<p ALIGN="justify">Las laminas planas salen al mercado en
											dos presentaciones: Laminas de 2.44X1.22m y Laminas de
											1.22x1m</p>
										<p ALIGN="justify">Disponibles sin recubrimiento, con
											barrera de vapor en foil de aluminio o papel kraft.</p>

									</td>
									<td style="width: 30%"><img
										src="images/ImagenesATA/20.jpg" class="img-responsive" alt="" />


									</td>
								</tr>

							</table>
						</div>
					</div>


					<div class="accordionItem close">
						<h2 class="accordionItemHeading">Especificaciones t�cnicas</h2>
						<div class="accordionItemContent">
							<table style="width: 100%">
								<tr>

									<td style="padding: 30px;"><img
										src="images/ImagenesATA/especificaciones.png"
										class="img-responsive" alt="" /></td>
								</tr>

							</table>
						</div>
					</div>

					<div class="close accordionItem ">
						<h2 class="accordionItemHeading">Espesores de aislamiento
							recomendados</h2>
						<div class="accordionItemContent">
							<table style="width: 100%">
								<tr>

									<td style="width: 30%; padding: 30px;"><img
										src="images/ImagenesATA/Espesor.png" class="img-responsive"
										alt="" /> <img src="images/ImagenesATA/Espesor2.png"
										class="img-responsive" alt="" /></td>
								</tr>

							</table>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>

				<div class="clearfix"></div>

			</div>
		</div>
	</div>
	<%@include file="/template/footer.html"%>
</body>
<script type="text/javascript" src="/js/jquery.acordeon.js">
	
</script>
</html>

