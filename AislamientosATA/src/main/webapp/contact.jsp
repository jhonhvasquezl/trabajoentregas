<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>

<script src="/js/jquery.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all">
<!--tages-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Sintony:400,700'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Fjalla+One'
	rel='stylesheet' type='text/css'>

<script src="/js/bootstrap.min.js"></script>


</head>
<body>
	<!--start-wrap-->
	<%@include file="/template/header.html"%>
	<div class="content">
		<div class="container">
			<!--start-contact-->
			<div class="contact" >
				<div class="section group" >
					<div class="col-md-4 col span_1_of_3">
						<div class="contact_info">
							<h3>BUSCAR</h3>
							<h5></h5>
							<div class="clearfix"></div>
							<div class="map">
								<iframe width="100%" height="175" frameborder="0" scrolling="no"
									marginheight="0" marginwidth="0"
									src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;sll=4.4950862,-74.1066692&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=18&amp;ll=4.4950862,-74.1066692&amp;output=embed"></iframe>
								<br> <small><a
									href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;sll=4.4950862,-74.1066692&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=18&amp;ll=4.4950862,-74.1066692"
									style="color: #666; text-align: left; font-size: 12px">Ver
										mapa grande</a></small>
							</div>
						</div>
						<div class="company_address">
							<h3>INFORMACI�N</h3>
							<h5></h5>
							<div class="clearfix"></div>
							<p>Calle 99 Sur No 5B-40 Este,</p>
							<p>Bogota D.C,</p>
							<p>Colombia</p>
							<p>Telefax:+57 (1) 761 33 53</p>
							<p>Celular: +57 311 442 2744</p>
							<p>
								Email: <span><a href="mailto:ata.ltda@hotmail.com">ata.ltda@hotmail.com</a></span>
							</p>
							<p>Asesor: Pedro Bar�n G.</p>
						</div>
					</div>
					<div class="col-md-8 col span_2_of_3" ng-app="app">
						<div class="contact-form" ng-controller="ContactController">
							<h3>CONT�CTENOS</h3>
							<h5></h5>
							<div class="clearfix"></div>
							<form name="formulario" method="get">
								<div>
									<span><label for="nombre">Nombre</label></span> <span><input
										type="text" id="nombre" name="nombre" ng-model="form.usuNombre" required ng-maxlength="200"></span>
										<span><p ng-show="formulario.nombre.$invalid" class="help-block">revisa tu informaci�n, algo no est� bien</p></span>
								</div>
								<div>
									<span><label for="apellido">Apellido</label></span> <span><input
										type="text" id="apellido" name="apellido" ng-model="form.usuApellido" required ng-maxlength="200"></span>
										<span><p ng-show="formulario.apellido.$invalid" class="help-block">revisa tu informaci�n, algo no est� bien</p></span>
								</div>
								<div>
									<span><label for="correo">E-Mail</label></span> <span><input
										id="correo" name="correo" type="text" ng-model="form.usuCorreo" required ng-maxlength="100" ng-pattern="exp"></span>
										<span><p ng-show="formulario.correo.$invalid" class="help-block">revisa tu informaci�n, algo no est� bien</p></span>
								</div>
								<div>
									<span><label for="celular">Celular</label></span> <span><input
										type="text"  id="celular" name="celular" ng-model="form.usuCelular" required ng-maxlength="10" ng-pattern="/^(\+?(\d{1}|\d{2}|\d{3})[- ]?)?\d{3}[- ]?\d{3}[- ]?\d{4}$/"> </span>
										<span><p ng-show="formulario.celular.$invalid" class="help-block">revisa tu informaci�n, algo no est� bien</p></span>
								</div>
								<div>
									<span><label for="mensaje">Asunto</label></span> <span><textarea
										rows="4" id="mensaje" name="mensaje" ng-model="form.usuMensaje" required ng-maxlength="512"> </textarea></span>
										<span><p ng-show="formulario.mensaje.$invalid" class="help-block">revisa tu informaci�n, algo no est� bien</p></span>
								</div>
								<div>
									<span><input type="submit" ng-click="submitbusqueda()"
										value="Guardar"></span>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
			<!--End-contact-->
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="loader"></div>
	<%@include file="/template/footer.html"%>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js"></script>
	<script src="js/controller.js"></script>
</body>
</html>

