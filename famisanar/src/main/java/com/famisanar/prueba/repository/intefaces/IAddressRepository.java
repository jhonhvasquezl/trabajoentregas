package com.famisanar.prueba.repository.intefaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.famisanar.prueba.repository.entities.Address;


public interface IAddressRepository extends JpaRepository<Address, Long> {

}
