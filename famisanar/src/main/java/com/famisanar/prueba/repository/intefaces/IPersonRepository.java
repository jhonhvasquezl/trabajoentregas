package com.famisanar.prueba.repository.intefaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.famisanar.prueba.repository.entities.Person;

public interface IPersonRepository extends JpaRepository<Person, Long> {

}
