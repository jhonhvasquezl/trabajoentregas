package com.famisanar.prueba.repository.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
@Table(name = "person")	
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="id")	
	private Long id;
	@Column(name="name")	
	private String name;
	@Column(name="phone_number")	
	private String phone_number;
	@Column(name="email")	
	private String email;
	@Column(name="student_number")	
	private String student_number;
	@Column(name="average_mark")	
	private String average_mark;
	@Column(name="salary")	
	private Double salary;
	@Column(name="type")	
	private Long type;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStudent_number() {
		return student_number;
	}
	public void setStudent_number(String student_number) {
		this.student_number = student_number;
	}
	public String getAverage_mark() {
		return average_mark;
	}
	public void setAverage_mark(String average_mark) {
		this.average_mark = average_mark;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	
	
	
}
