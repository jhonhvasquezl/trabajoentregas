package com.famisanar.prueba.usecase.implement;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.famisanar.prueba.repository.entities.Person;
import com.famisanar.prueba.repository.intefaces.IPersonRepository;
import com.famisanar.prueba.usecase.interfaces.IPersonCase;

@Service	
public class PersonCase implements IPersonCase{

	@Autowired
	IPersonRepository aw;
	


	@Override
	public Person savePerson(Person person) {
		// TODO Auto-generated method stub
		return aw.save(person);
	}

	@Override
	public Person getPersonById(Long id) {
		// TODO Auto-generated method stub
		return aw.getOne(id);
	}

	@Override
	public Person editPerson(Person person) {
		// TODO Auto-generated method stub
		//person= aw.getOne(person.getId());
		return aw.save(person);
	}

	@Override
	public void removePerson(Long id) {
		// TODO Auto-generated method stub
		aw.deleteById(id);
	}

	

}
