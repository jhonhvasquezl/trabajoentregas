package com.famisanar.prueba.usecase.interfaces;

import java.util.List;

import com.famisanar.prueba.repository.entities.Address;


public interface IAddressCase {

	
	public List<Address> getAddress();
	public Address saveAddress(Address Address);
	public Address getAddressById(Long id);
	public List<Address> getAddressById(Iterable<Long> ids);
	public void deleteAddressId(Long id); 
	
}
