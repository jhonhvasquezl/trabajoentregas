package com.famisanar.prueba.usecase.interfaces;


import com.famisanar.prueba.repository.entities.Person;


public interface IPersonCase {

	public Person savePerson(Person person);
	public Person getPersonById(Long id);
	public Person editPerson(Person person);
	public void removePerson(Long id);
	

}
