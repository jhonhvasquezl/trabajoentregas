package com.famisanar.prueba.usecase.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.famisanar.prueba.repository.entities.Address;
import com.famisanar.prueba.repository.intefaces.IAddressRepository;
import com.famisanar.prueba.usecase.interfaces.IAddressCase;

@Service	
public class AddressCase implements IAddressCase {

	@Autowired
	IAddressRepository aw;
	@Override
	public List<Address> getAddress() {
		// TODO Auto-generated method stub
		return aw.findAll();
	}

	@Override
	public Address saveAddress(Address address) {
		// TODO Auto-generated method stub
		return aw.save(address);
	}

	@Override
	public Address getAddressById(Long id) {
		// TODO Auto-generated method stub
		return aw.getOne(id);
	}

	@Override
	public List<Address> getAddressById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return aw.findAllById(ids);
	}
	@Override
	public void deleteAddressId(Long id) {
		// TODO Auto-generated method stub
		 aw.deleteById(id);
	}

}
