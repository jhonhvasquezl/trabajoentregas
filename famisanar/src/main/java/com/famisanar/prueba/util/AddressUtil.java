package com.famisanar.prueba.util;

import com.famisanar.prueba.controller.dto.AddressDTO;
import com.famisanar.prueba.repository.entities.Address;

public class AddressUtil {

	public static Address objModelToDto(AddressDTO dto){
		Address ent = new Address();
		ent.setCity(dto.getCity());
		ent.setCountry(dto.getCountry());
		ent.setPostal_code(dto.getPostal_code());
		ent.setStreet(dto.getStreet());
		ent.setId(dto.getId());
		return ent;
	}
	public static AddressDTO objDtoToModel(Address dto){
		AddressDTO ent = new AddressDTO();
		ent.setCity(dto.getCity());
		ent.setCountry(dto.getCountry());
		ent.setPostal_code(dto.getPostal_code());
		ent.setStreet(dto.getStreet());
		ent.setId(dto.getId());
		return ent;
	}
}
