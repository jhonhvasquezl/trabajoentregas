package com.famisanar.prueba.util;

import com.famisanar.prueba.controller.dto.ProfessorDTO;
import com.famisanar.prueba.controller.dto.StudentDTO;
import com.famisanar.prueba.repository.entities.Person;

public class PersonUtil {

	
	public static ProfessorDTO objDtoToModelPf(Person person){
		ProfessorDTO dto= new ProfessorDTO();
		dto.setId(person.getId());
		dto.setName(person.getName());
		dto.setPhone_number(person.getPhone_number());
		dto.setEmail(person.getEmail());
		dto.setSalary(person.getSalary());
		dto.setType(person.getType());

		return dto;
	}

	public static Person objModelToPf(ProfessorDTO professorDTO){
		Person ent = new Person();
		ent.setId(professorDTO.getId());
		ent.setName(professorDTO.getName());
		ent.setPhone_number(professorDTO.getPhone_number());
		ent.setEmail(professorDTO.getEmail());
		ent.setSalary(professorDTO.getSalary());
		ent.setType(professorDTO.getType());

		return ent;
	}
	
	public static StudentDTO objDtoToModelSt(Person person){
		StudentDTO dto= new StudentDTO();		
		dto.setId(person.getId());
		dto.setName(person.getName());
		dto.setPhone_number(person.getPhone_number());
		dto.setEmail(person.getEmail());
		dto.setAverage_mark(person.getAverage_mark());
		dto.setStudent_number(person.getStudent_number());
		dto.setType(person.getType());

		return dto;
	}

	public static Person objModelToSt(StudentDTO dto){
		Person ent = new Person();
		ent.setId(dto.getId());
		ent.setName(dto.getName());
		ent.setPhone_number(dto.getPhone_number());
		ent.setEmail(dto.getEmail());
		ent.setAverage_mark(dto.getAverage_mark());
		ent.setStudent_number(dto.getStudent_number());
		ent.setType(dto.getType());

		return ent;
	}
}
