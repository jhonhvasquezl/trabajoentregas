package com.famisanar.prueba.controller.interfaces;

import com.famisanar.prueba.controller.dto.ProfessorDTO;
import com.famisanar.prueba.controller.dto.StudentDTO;

public interface IPersonController {

	
	void createProfessor(ProfessorDTO professorDTO);
	void editProfessor(ProfessorDTO professorDTO);
	void createStudent(StudentDTO studentDTO);
	void editStudent(StudentDTO studentDTO);
	void deletePerson(Long id);
	ProfessorDTO readProfessor(Long id);
	StudentDTO readStudent(Long id);
}
