package com.famisanar.prueba.controller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.famisanar.prueba.controller.dto.ProfessorDTO;
import com.famisanar.prueba.controller.dto.StudentDTO;
import com.famisanar.prueba.controller.interfaces.IPersonController;
import com.famisanar.prueba.usecase.interfaces.IAddressCase;
import com.famisanar.prueba.usecase.interfaces.IPersonCase;
import com.famisanar.prueba.util.AddressUtil;
import com.famisanar.prueba.util.PersonUtil;

@RestController
public class PersonController implements IPersonController {
	
	@Autowired
	IPersonCase pc;
	@Autowired
	IAddressCase ac;
	
	@Override
	@RequestMapping(value = "/createProfessor", method = RequestMethod.POST, produces = "application/json")
	public void createProfessor(@RequestBody ProfessorDTO professorDTO) {
		
		if(professorDTO.getAddressDTO()!=null)
		{
			professorDTO.setType(ac.saveAddress(AddressUtil.objModelToDto(professorDTO.getAddressDTO())).getId());
		}
		pc.savePerson(PersonUtil.objModelToPf(professorDTO));
		// TODO Auto-generated method stub
		
	}

	@Override
	@RequestMapping(value = "/editProfessor", method = RequestMethod.PUT, produces = "application/json")
	public void editProfessor(@RequestBody ProfessorDTO professorDTO) {
		// TODO Auto-generated method stub
		
		pc.editPerson(PersonUtil.objModelToPf(professorDTO));
		ac.saveAddress(AddressUtil.objModelToDto(professorDTO.getAddressDTO()));
		
	}

	@Override
	@RequestMapping(value = "/createStudent", method = RequestMethod.POST, produces = "application/json")
	public void createStudent(@RequestBody StudentDTO studentDTO) {
		// TODO Auto-generated method stub
		if(studentDTO.getAddressDTO()!=null)
		{
			studentDTO.setType(ac.saveAddress(AddressUtil.objModelToDto(studentDTO.getAddressDTO())).getId());
		}
		pc.savePerson(PersonUtil.objModelToSt(studentDTO));

	}

	@Override
	@RequestMapping(value = "/editStudent", method = RequestMethod.PUT, produces = "application/json")
	public void editStudent(@RequestBody StudentDTO studentDTO) {
		// TODO Auto-generated method stub
		pc.editPerson(PersonUtil.objModelToSt(studentDTO));
		ac.saveAddress(AddressUtil.objModelToDto(studentDTO.getAddressDTO()));

	}

	@Override
	@RequestMapping(value = "/deletePerson/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deletePerson(@PathVariable Long id) {
		// TODO Auto-generated method stub
		pc.removePerson(id);
		ProfessorDTO pf = PersonUtil.objDtoToModelPf(pc.getPersonById(id));
		if(pf.getType()!=null)
		{
			ac.deleteAddressId(pf.getType());
		}

	}

	@Override
	@RequestMapping(value = "/readProfessor/{id}", method = RequestMethod.GET, produces = "application/json")
	public ProfessorDTO readProfessor(@PathVariable Long id) {
		// TODO Auto-generated method stub
		ProfessorDTO pf = PersonUtil.objDtoToModelPf(pc.getPersonById(id));
		if(pf.getType()!=null)
		{
			pf.setAddressDTO(AddressUtil.objDtoToModel(ac.getAddressById(pf.getType())));
		}
		return pf;
	}

	@Override
	@RequestMapping(value = "/readStudent/{id}", method = RequestMethod.GET, produces = "application/json")
	public StudentDTO readStudent(@PathVariable Long id) {
		// TODO Auto-generated method stub
		StudentDTO pf = PersonUtil.objDtoToModelSt(pc.getPersonById(id));
		if(pf.getType()!=null)
		{
			pf.setAddressDTO(AddressUtil.objDtoToModel(ac.getAddressById(pf.getType())));
		}
		return pf;
	}

	
}
