package com.famisanar.prueba.controller.dto;

public class StudentDTO  extends PersonDTO{

	private String student_number;
	private String average_mark;
	
	public String getStudent_number() {
		return student_number;
	}
	public void setStudent_number(String student_number) {
		this.student_number = student_number;
	}
	public String getAverage_mark() {
		return average_mark;
	}
	public void setAverage_mark(String average_mark) {
		this.average_mark = average_mark;
	}
	
}
