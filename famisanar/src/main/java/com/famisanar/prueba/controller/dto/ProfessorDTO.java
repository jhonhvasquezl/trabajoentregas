package com.famisanar.prueba.controller.dto;

public class ProfessorDTO extends PersonDTO {

	private Double salary;

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}


}
