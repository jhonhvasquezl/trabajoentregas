package biz.solredes.veedurias.controller;

import java.util.ArrayList;
import java.util.List;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class InidiraVeeduriaFiltrosEspeciales extends ActionBarActivity {
	private int id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inirida_veeduria_filtros_especiales);
		listadofiltros();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(InidiraVeeduriaFiltrosEspeciales.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void listadofiltros() {
		String filtro = selector();
		ListView listView = (ListView) findViewById(R.id.listadodefiltrosespeciales);
		Intent cat = getIntent();
		String secretaria = cat.getExtras().getString("nuevasecretaria");
		List<String> listado = new AbstractDaoOra()
				.getDescripcionfiltroespecial(secretaria, filtro);

		if (listado.size() != 0) {

			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, listado);

			listView.setAdapter(arrayAdapter);
			listView.setTextFilterEnabled(true);
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View view,
						int i, long arg3) {
					String nombredelproyecto;
					nombredelproyecto = adapterView.getItemAtPosition(i)
							.toString();
					Toast.makeText(
							getBaseContext(),
							"" + "Proyecto: "
									+ adapterView.getItemAtPosition(i),
							Toast.LENGTH_LONG).show();
					Intent myIntent1 = new Intent(view.getContext(),
							IniridaVeeduriaProyecto.class);
					myIntent1.putExtra("nombreproyecto", nombredelproyecto);
					startActivityForResult(myIntent1, 0);

				}

			});
		} else {
			List<String> listadov = new ArrayList<String>();
			listadov.add("Ningun Proyecto Disponible");
			ListView listadovoid = (ListView) findViewById(R.id.listadodesocupado);
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, listadov);
			listadovoid.setAdapter(arrayAdapter);
			listadovoid.setTextFilterEnabled(true);
		}

	}

	public String selector() {
		String categoriaespecifica = "";
		Intent cat = getIntent();
		int popular = cat.getExtras().getInt("popular");
		int reciente = cat.getExtras().getInt("reciente");
		int antiguo = cat.getExtras().getInt("antiguo");
		TextView titulofiltro = (TextView) findViewById(R.id.cogidocontrato);
		if (popular != 0) {
			titulofiltro.setText("Proyectos Populares");
			categoriaespecifica = "PRY_PROMEDIO_CAL";
		} else if (reciente != 0) {
			titulofiltro.setText("Nuevos Proyectos");
			categoriaespecifica = "PRY_FECHA_RAD ASC";
		} else if (antiguo != 0) {
			titulofiltro.setText("Antiguos Proyectos");
			categoriaespecifica = "PRY_FECHA_RAD DESC";
		}

		return categoriaespecifica;

	}
}
