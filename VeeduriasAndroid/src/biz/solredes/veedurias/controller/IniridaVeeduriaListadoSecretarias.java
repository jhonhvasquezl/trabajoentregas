package biz.solredes.veedurias.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import biz.solredes.veedurias.model.dao.ConnectOra;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class IniridaVeeduriaListadoSecretarias extends ActionBarActivity{
	public String Secretariaespecial="";
	public ListView listView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inirida_veeduria_listado_secretarias);
		listadosecretarias();

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(IniridaVeeduriaListadoSecretarias.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
    public void listadosecretarias()
    {
    	 listView = (ListView) findViewById(R.id.listadosecretarias);
         List<String> listado = new AbstractDaoOra().getDescripcionsecretarias();

         ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                 this,android.R.layout.simple_list_item_1,listado );
         listView.setAdapter(arrayAdapter);
         listView.setTextFilterEnabled(true);
         listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

             @Override
             public void onItemClick(AdapterView<?> adapterView, View view, int i,
                                     long arg3) {
     		 	String secretaria;
    		 	secretaria= adapterView.getItemAtPosition(i).toString();

     	        Toast.makeText(getBaseContext(), "" +  "Secretarias: " + adapterView.getItemAtPosition(i),
     	        Toast.LENGTH_LONG).show();
    	        Intent myIntent1 = new Intent(view.getContext(), IniridaVeeduriaListadoCategorias.class);
    	        myIntent1.putExtra("secretaria", secretaria);
                startActivityForResult(myIntent1, 0);

             }



         });

    }
    
	
}
