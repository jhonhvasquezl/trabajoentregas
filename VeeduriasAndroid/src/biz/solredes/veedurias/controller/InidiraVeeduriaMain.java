package biz.solredes.veedurias.controller;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class InidiraVeeduriaMain extends ActionBarActivity {
	public ImageButton image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inidira_veeduria_main);
		AbstractDaoOra con = new AbstractDaoOra();

		image = (ImageButton) findViewById(R.id.imageprincipal);
		image.setImageResource(R.drawable.inicioimg);
		engresarabusqueda();
		if (getIntent().getBooleanExtra("EXIT", false)) {
			finish();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (item.getItemId()) {
        	case R.id.item1:
				Intent pasoabusqueda = new Intent(InidiraVeeduriaMain.this,
						IniridaVeeduriaBuscar.class);
				startActivity(pasoabusqueda);

            case R.id.salir:
            	 finish();
                 return true;
          default:return super.onOptionsItemSelected(item);
          }
    }

	public void engresarabusqueda() {
		image = (ImageButton) findViewById(R.id.imageprincipal);
		image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent pasoabusqueda = new Intent(InidiraVeeduriaMain.this,
						IniridaVeeduriaBuscar.class);
				startActivity(pasoabusqueda);
			}
		});

	}

	public void salir() {
		Toast toast1 = Toast.makeText(getApplicationContext(),
				"Problemasde conexion (Revisa tu conexion a internet)",
				Toast.LENGTH_SHORT);
		toast1.show();
		Intent intent3 = new Intent(getApplicationContext(),
				InidiraVeeduriaMain.class);
		intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent3.putExtra("EXIT", true);
		startActivity(intent3);
	}

}
