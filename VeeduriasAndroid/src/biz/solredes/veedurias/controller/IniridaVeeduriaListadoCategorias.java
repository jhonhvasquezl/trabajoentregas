package biz.solredes.veedurias.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import biz.solredes.veedurias.model.dao.ConnectOra;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class IniridaVeeduriaListadoCategorias extends ActionBarActivity{
	
	public ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inirida_veeduria_listado_categorias);
		engresaraantiguo();
		engresarapopulares();
		engresararecientes();
		listadocategorias();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(IniridaVeeduriaListadoCategorias.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	public void engresaraantiguo(){
        ImageButton entrada= (ImageButton)findViewById(R.id.antiguos_boton);
        entrada.setBackground(null);
        entrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           	 	int antiguo=1;
            	Intent cat = getIntent();
           	 	String secretaria = cat.getExtras().getString("secretaria");
           	 	Intent  pasoacategorias= new Intent(IniridaVeeduriaListadoCategorias.this,InidiraVeeduriaFiltrosEspeciales.class);
           	 	pasoacategorias.putExtra("nuevasecretaria", secretaria);
           	 	pasoacategorias.putExtra("antiguo", antiguo);
           	 	startActivity(pasoacategorias);

            }
        });

    }
    public void engresararecientes(){
        ImageButton entrada= (ImageButton)findViewById(R.id.recientes_boton);
        entrada.setBackground(null);
        entrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            int reciente= 2;
           	 Intent cat = getIntent();
     		 String secretaria = cat.getExtras().getString("secretaria");
             Intent  pasoacategorias= new Intent(IniridaVeeduriaListadoCategorias.this,InidiraVeeduriaFiltrosEspeciales.class);
             pasoacategorias.putExtra("nuevasecretaria", secretaria);
             
			 pasoacategorias.putExtra("reciente", reciente);
              startActivity(pasoacategorias);

            }
        });}
    public void engresarapopulares(){
    	ImageButton entrada= (ImageButton)findViewById(R.id.populares_boton);
    	entrada.setBackground(null);
    	entrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           	 int popular=3;
            	Intent cat = getIntent();
     		 String secretaria = cat.getExtras().getString("secretaria");
              Intent  pasoacategorias= new Intent(IniridaVeeduriaListadoCategorias.this,InidiraVeeduriaFiltrosEspeciales.class);
              pasoacategorias.putExtra("nuevasecretaria", secretaria);
              pasoacategorias.putExtra("popular", popular);
              startActivity(pasoacategorias);

            }
        });}
    public void listadocategorias()
    {
    	 listView = (ListView) findViewById(R.id.listado_categorias);
    	 Intent cat = getIntent();
 		 String secretaria = cat.getExtras().getString("secretaria");
         List<String> listado = new AbstractDaoOra().getDescripcioncategoria(secretaria);

         ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                 this,android.R.layout.simple_list_item_1,listado );
         listView.setAdapter(arrayAdapter);
         listView.setTextFilterEnabled(true);
         listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

             @Override
             public void onItemClick(AdapterView<?> adapterView, View view, int i,
                                     long arg3) {
     		 	String categoria;
     		 	categoria= adapterView.getItemAtPosition(i).toString();
     	        Toast.makeText(getBaseContext(), "" +  "Categoria : " + adapterView.getItemAtPosition(i),
     	        Toast.LENGTH_LONG).show();
    	        Intent myIntent1 = new Intent(view.getContext(), IniridaVeeduriaListadoFiltros.class);
    	        myIntent1.putExtra("categoria", categoria);
                startActivityForResult(myIntent1, 0);

                
             }



         });

    }
    

}
