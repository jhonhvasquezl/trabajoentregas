package biz.solredes.veedurias.controller;




import biz.solredes.veedurias.R;

import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class IniridaVeeduriaProyecto extends ActionBarActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);


		informacionproyecto();
//		engresaraaprybuscar();
//		engresararprysecretarias();
		engresaraaprycomentarios();
		engresaraaprycomentarioshacer();

		
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(IniridaVeeduriaProyecto.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
//	public void engresaraaprybuscar(){
//        Button antiguedad= (Button)findViewById(R.id.b_buscar);
//        antiguedad.setBackground(null);
//        antiguedad.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                 Intent antiguo= new Intent(IniridaVeeduriaProyecto.this,IniridaVeeduriaBuscar.class);
//                startActivity(antiguo);
//            }
//        }); 

//    }
	public void engresaraaprycomentarios(){
        ImageButton antiguedad= (ImageButton)findViewById(R.id.ir_comentario);
        antiguedad.setBackground(null);
        antiguedad.setImageResource(R.drawable.icon_ver_comentarios);
        antiguedad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        		Intent i = getIntent();
        		String nombrepryt = i.getExtras().getString("nombreproyecto");
                Intent comentarios= new Intent(IniridaVeeduriaProyecto.this,IniridaVeeduriaComentarios.class);
                comentarios.putExtra("nombreproyectos", nombrepryt);
                startActivity(comentarios);
            }
        }); 

    }
	public void engresaraaprycomentarioshacer(){
        
		ImageButton antiguedad = (ImageButton)findViewById(R.id.hacer_comentario);
        antiguedad.setBackground(null);
        antiguedad.setImageResource(R.drawable.incon_comentarios);
        antiguedad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        		Intent i = getIntent();
        		String nombrepryt = i.getExtras().getString("nombreproyecto");
                Intent comentarios= new Intent(IniridaVeeduriaProyecto.this,IniridaVeeduriaRealizarDetalle.class);
                comentarios.putExtra("nombreproyectos", nombrepryt);
                startActivity(comentarios);
            }
        }); 

    }
	
//    public void engresararprysecretarias(){
//        Button recientesb= (Button)findViewById(R.id.b_secretarias);
//        recientesb.setBackground(null);
//        recientesb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                 Intent reciente= new Intent(IniridaVeeduriaProyecto.this,IniridaVeeduriaListadoSecretarias.class);
//                startActivity(reciente);
//
//            }
//        });}
	public void informacionproyecto()
	{
		AbstractDaoOra con =new AbstractDaoOra();
		Intent i = getIntent();
		String nombrepryt = i.getExtras().getString("nombreproyecto");
		con.proyectodata(nombrepryt);
		setContentView(R.layout.activity_inirida_veeduria_proyecto_main);
		TextView nombre= (TextView) findViewById(R.id.l_nombreproyectos);
		TextView descripcion= (TextView) findViewById(R.id.l_descripcion);
		TextView estado= (TextView) findViewById(R.id.l_estado);
		TextView codigo= (TextView) findViewById(R.id.cogidocontrato);
		TextView duracion=(TextView) findViewById(R.id.duracion);
		TextView fechainicio= (TextView) findViewById(R.id.fechainicio);
		TextView fechafin= (TextView) findViewById(R.id.fechafin); 
		TextView valor = (TextView) findViewById(R.id.precio);
		TextView avancefinanciero= (TextView) findViewById(R.id.avancefinanciero);
		TextView avancefisico=(TextView) findViewById(R.id.avancefisico);
		TextView localizacion= (TextView) findViewById(R.id.localizacion);
		TextView financiacion= (TextView) findViewById(R.id.financiacion);
		TextView pais= (TextView) findViewById(R.id.pais);
		TextView departamento = (TextView) findViewById(R.id.departamento);
		TextView municipio= (TextView) findViewById(R.id.municipio);
		ImageView fotoproyecto =(ImageView) findViewById(R.id.imagenproyecto);
		TextView lugar = (TextView) findViewById(R.id.lugar);
		TextView  indicadores = (TextView) findViewById(R.id.indicadores);
		RatingBar promedio= (RatingBar) findViewById(R.id.promediocalificacion);
		TextView dependencia=(TextView) findViewById(R.id.secretarialabel);
		TextView secreatia=(TextView) findViewById(R.id.textView18);
		TextView meta = (TextView) findViewById(R.id.meta);

		

		fotoproyecto.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		fotoproyecto.setAdjustViewBounds(true);
		fotoproyecto.setImageBitmap(BitmapFactory.decodeStream(con.getFoto()));
		
		nombre.setText(con.getNombre());
		descripcion.setText(con.getDescripcion());
		estado.setText(con.getEstado());
		codigo.setText(con.getCodigocontrato());
		duracion.setText(con.getDuracion());
		fechainicio.setText(con.getFechainicio());
		fechafin.setText(con.getFechafin());
		valor.setText(con.getValor());
		avancefinanciero.setText(con.getAvancefinanciero());
		avancefisico.setText(con.getAvancefisico());
		localizacion.setText(con.getLocalizacion());
		financiacion.setText(con.getFinanciacion());
		pais.setText(con.getPais());
		departamento.setText(con.getDepartamento());
		municipio.setText(con.getMunicipio());
		lugar.setText(con.getLugar());
		indicadores.setText(con.getIndicadores());
		promedio.setRating(con.getCalificacion());
		dependencia.setText(con.getDependencia());
		secreatia.setText(con.getSecretaria());
		meta.setText(con.getMeta());

	}
	

   
}
