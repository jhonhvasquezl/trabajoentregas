package biz.solredes.veedurias.controller;

import java.util.ArrayList;
import java.util.List;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

public class IniridaVeeduriaBuscar extends ActionBarActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener{
	public AutoCompleteTextView autoCompleteTextView =null;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inirida_veeduria_busqueda);
		listadobusqueda();
		engresarasecretarias();
		mostrarproyectos();
		if (getIntent().getBooleanExtra("EXIT", false)) {
			finish();
		}

	}
	
	 @Override
	    public void onItemClick(AdapterView<?> adapterView , View view, int i, long l) {
		 	String nombredelproyecto;
		 	nombredelproyecto= adapterView.getItemAtPosition(i).toString();
	        Toast.makeText(getBaseContext(), "" +  "Proyecto: " + adapterView.getItemAtPosition(i),
	        Toast.LENGTH_LONG).show();
	        Intent myIntent1 = new Intent(view.getContext(), IniridaVeeduriaProyecto.class);
	        myIntent1.putExtra("nombreproyecto", nombredelproyecto);
            startActivityForResult(myIntent1, 0);
            
            
	    }

	    @Override
	    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

	    }

	    @Override
	    public void onNothingSelected(AdapterView<?> adapterView) {
	        InputMethodManager imm = (InputMethodManager) getSystemService(
	                INPUT_METHOD_SERVICE);
	        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	    }
	    public void listadobusqueda()
	    {	    
	    	String item[];
	    	ArrayList<String> listabusquedaarray = new AbstractDaoOra().getDescripcionbusqueda();
	        item= new String[listabusquedaarray.size()];
	        item =listabusquedaarray.toArray(item);
	    	autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.l_busqueda);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, item);
	        autoCompleteTextView.setThreshold(1);
	        autoCompleteTextView.setAdapter(adapter);
	        autoCompleteTextView.setOnItemSelectedListener(this);
	        autoCompleteTextView.setOnItemClickListener(this);
	    }
	    
	   
	    
	    public void mostrarproyectos()
	    {
	    	 ImageButton entradas= (ImageButton)findViewById(R.id.bucartodoslosproyectosecretarias);
	    	 entradas.setBackground(null);
	    	 entradas.setImageResource(R.drawable.proyectos);
	    	 entradas.setOnClickListener(new View.OnClickListener() {
	             @Override
	             public void onClick(View view) {
	            	 
	            	 	Intent  pasoacategorias= new Intent(IniridaVeeduriaBuscar.this,IniridaaVeeduriaTodosLosProyectos.class);
	            	 	startActivity(pasoacategorias);

	             }
	         });
	    }


		public void engresarasecretarias(){
	        ImageButton entrada= (ImageButton)findViewById(R.id.pasoasecretarias);
	        entrada.setBackground(null);	        
	        entrada.setImageResource(R.drawable.incon_secretaria);
	        entrada.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	              Intent  pasoacategorias= new Intent(IniridaVeeduriaBuscar.this,IniridaVeeduriaListadoSecretarias.class);
	                startActivity(pasoacategorias);

	            }
	        });

	    }
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.inidira_veeduria_second, menu);
	        return true;
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        int id = item.getItemId();
	        switch (item.getItemId()) {

	            case R.id.salir:
//	            	Intent intent3 = new Intent(getApplicationContext(), InidiraVeeduriaMain.class);
//	            	intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	            	intent3.putExtra("EXIT", true);
//	            	startActivity(intent3);
//	            	return true;
	            	Intent intent3 = new Intent(getApplicationContext(),
	    					IniridaVeeduriaBuscar.class);
	    			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    			intent3.putExtra("EXIT", true);
	    			startActivity(intent3);
	    			return true;
	          default:return super.onOptionsItemSelected(item);
	          }
	    }
	    
	   

	    

}
