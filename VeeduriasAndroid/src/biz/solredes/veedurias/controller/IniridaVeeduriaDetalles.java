package biz.solredes.veedurias.controller;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class IniridaVeeduriaDetalles extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inirida_veeduria_detalles);
		detallescomentarios();
	}
	public void detallescomentarios() 
	{

		AbstractDaoOra con =new AbstractDaoOra();
		Intent nombrepry = getIntent();
		String nombrepryt = nombrepry.getExtras().getString("nombreproyectos");
		String comentario = nombrepry.getExtras().getString("comentario");
		con.detalledata(nombrepryt, comentario);
		TextView nombre =(TextView) findViewById(R.id.nombre_detalle);
		TextView detalle=(TextView) findViewById(R.id.descripcion_detalle);
		RatingBar calificacion =(RatingBar) findViewById(R.id.calificacion_detalle);
		ImageView fotoproyecto =(ImageView) findViewById(R.id.imagen_comentario);

		fotoproyecto.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		fotoproyecto.setAdjustViewBounds(true);
		fotoproyecto.setImageBitmap(BitmapFactory.decodeStream(con.getFoto()));
		nombre.setText(con.getNombre());
		detalle.setText(con.getDescripcion());
		calificacion.setRating(con.getCalificacion());
				
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(IniridaVeeduriaDetalles.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
