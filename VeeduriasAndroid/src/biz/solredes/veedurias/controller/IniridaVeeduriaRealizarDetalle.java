package biz.solredes.veedurias.controller;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.ConnectOra;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class IniridaVeeduriaRealizarDetalle extends ActionBarActivity {
	private static int RESULT_LOAD_IMAGE = 1;
	public byte imagendata[];
	private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inirida_veeduria_comentarios_hacer);
		Button subir = (Button) findViewById(R.id.seleccionar_una_img);

		subir.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, RESULT_LOAD_IMAGE);

			}
		});

		Intent i = getIntent();
		String nombrepryt = i.getExtras().getString("nombreproyectos");
		TextView labeltales = (TextView) findViewById(R.id.listadoproyectohacercomentario);
		labeltales.setText(nombrepryt);

		Button botonInsertar = (Button) findViewById(R.id.enviar_comentarios);
		botonInsertar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent i = getIntent();
				String nombrepryt = i.getExtras().getString("nombreproyectos");
				EditText nombresitos = (EditText) findViewById(R.id.c_nombre);
				EditText correitos = (EditText) findViewById(R.id.c_correo);
				EditText telefonitos = (EditText) findViewById(R.id.c_telefono);
				EditText comentaritos = (EditText) findViewById(R.id.c_comentarios);
				RatingBar calificacion = (RatingBar) findViewById(R.id.c_calificacion);
				ConnectOra conn = new ConnectOra("192.168.0.45", "DBMASTER",
						"C##DARIEN2015_INIRIDA", "DARIEN2015_INIRIDA");

				String sql = "INSERT INTO S_PRY_COMENTARIOS(PRCOM_FOTO,PRY_ID,PRCOM_NOMBRE,PRCOM_COMENTARIO,PRCOM_CALIFICACION,PRCOM_TELEFONO,PRCOM_CORREO) VALUES (?,(SELECT  ps.PRY_ID FROM S_PROYECTO ps  WHERE ps.PRY_OBJETO  = '"
						+ nombrepryt
						+ "'  ),'"
						+ nombresitos.getText().toString()
						+ "','"
						+ comentaritos.getText().toString()
						+ "',"
						+ calificacion.getRating()
						+ ",'"
						+ telefonitos.getText().toString()
						+ "','"
						+ correitos.getText().toString() + "')";
				boolean tal = validateEmail(correitos.getText().toString());
				boolean existeNombre;
				if(nombresitos.getText().toString().equals("")){
					existeNombre = false;
				} else {
					existeNombre = true;
				}
				boolean existeCalificacion;
				if(calificacion.getRating() == 0){
					existeCalificacion = false;
				} else {
					existeCalificacion = true;
				}
				boolean existeComentario;
				if(comentaritos.getText().toString().equals("")){
					existeComentario = false;
				} else {
					existeComentario = true;
				}
				

				try {
					if (existeCalificacion && existeNombre && existeComentario) {
						PreparedStatement se = conn.getConexion()
								.prepareStatement(sql);
						System.out.println("Proyecto: " + getImagendata() + "");
						se.setBytes(1, getImagendata());

						se.executeUpdate();
						Toast toast1 = Toast.makeText(getApplicationContext(),
								"Comentario Relizado Con Exito",
								Toast.LENGTH_SHORT);
						toast1.show();

						finish();
					} else { 
						if(tal == false){
							Toast toast1 = Toast
									.makeText(
											getApplicationContext(),
											"Error al realizar el comentario (Revisa tu correo electronico): " + correitos.getText().toString(),
											Toast.LENGTH_SHORT);
							toast1.show();
						}
						if(existeCalificacion == false){
							Toast toast1 = Toast
									.makeText(
											getApplicationContext(),
											"Error al realizar el comentario (Escribe una calificaci�n): ",
											Toast.LENGTH_SHORT);
							toast1.show();
						}
						if (existeNombre == false) {
							Toast toast1 = Toast
									.makeText(
											getApplicationContext(),
											"Error al realizar el comentario (Escribe t� nombre): ",
											Toast.LENGTH_SHORT);
							toast1.show();
						} 
						if (existeComentario == false) {
							Toast toast1 = Toast
									.makeText(
											getApplicationContext(),
											"Error al realizar el comentario (Escribe alg�n comentario): ",
											Toast.LENGTH_SHORT);
							toast1.show();
						}
					}
				} catch (SQLException e) {
					Toast toast1 = Toast.makeText(getApplicationContext(),
							"Error al realizar el comentario. ",
							Toast.LENGTH_SHORT);
					toast1.show();
					System.out.println(e.toString());

				}

			}

		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
				&& null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			cursor.close();

			FileInputStream in;
			BufferedInputStream buf;
			try {

				in = new FileInputStream(picturePath);
				buf = new BufferedInputStream(in);
				byte[] bMapArray = new byte[buf.available()];
				buf.read(bMapArray);
				setImagendata(bMapArray);
				Bitmap bMap = BitmapFactory.decodeByteArray(bMapArray, 0,
						bMapArray.length);

				ImageView imagen = (ImageView) findViewById(R.id.imagen_cargada);
				imagen.setImageBitmap(bMap);

				if (in != null) {
					in.close();
				}
				if (buf != null) {
					buf.close();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	public static boolean validateEmail(String email) {

		// Compiles the given regular expression into a pattern.
		Pattern pattern = Pattern.compile(PATTERN_EMAIL);

		// Match the given input against this pattern
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();

	}

	public byte[] getImagendata() {
		return imagendata;
	}

	public void setImagendata(byte[] imagendata) {
		this.imagendata = imagendata;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(
					IniridaVeeduriaRealizarDetalle.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
