package biz.solredes.veedurias.controller;

import java.util.List;

import biz.solredes.veedurias.R;
import biz.solredes.veedurias.model.dao.AbstractDaoOra;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class IniridaVeeduriaComentarios extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inirida_veeduria_comentarios);
		listadocomentarios();
	}

	public void listadocomentarios() {

		Intent i = getIntent();
		String nombrepryt = i.getExtras().getString("nombreproyectos");
		TextView listadotext = (TextView) findViewById(R.id.categoria_proyecto_comentarios);
		listadotext.setText("Comentarios de: "+nombrepryt);
		ListView listView = (ListView) findViewById(R.id.listado_de_comentario);
		List<String> listado = new AbstractDaoOra()
				.getDescripcioncomentarios(nombrepryt);

		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, listado);
		listView.setAdapter(arrayAdapter);
		listView.setTextFilterEnabled(true);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int i, long arg3) {
				String comentario;
				comentario = adapterView.getItemAtPosition(i).toString();
				Intent nombrepry = getIntent();
				String nombrepryt = nombrepry.getExtras().getString("nombreproyectos");
				Toast.makeText(
						getBaseContext(),
						"" + "Comentario : " + adapterView.getItemAtPosition(i),
						Toast.LENGTH_LONG).show();
				Intent myIntent1 = new Intent(view.getContext(),
						IniridaVeeduriaDetalles.class);
				myIntent1.putExtra("nombreproyectos", nombrepryt);
				myIntent1.putExtra("comentario",comentario);
				startActivityForResult(myIntent1, 0);

			}

		});

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inidira_veeduria_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		 int id = item.getItemId();
		switch (item.getItemId()) {
		case R.id.item1:
			Intent pasoabusqueda = new Intent(IniridaVeeduriaComentarios.this,
					IniridaVeeduriaBuscar.class);
			startActivity(pasoabusqueda);
			return true;
			
		case R.id.salir:
			Intent intent3 = new Intent(getApplicationContext(),
					IniridaVeeduriaBuscar.class);
			intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent3.putExtra("EXIT", true);
			startActivity(intent3);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
