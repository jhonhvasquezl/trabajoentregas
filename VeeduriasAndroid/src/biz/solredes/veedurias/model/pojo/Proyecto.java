package biz.solredes.veedurias.model.pojo;

public class Proyecto {
	long id;
	String nombreProyecto;
	String descripcionProyecto;
	byte[] fotoProyecto;
	
	public String getNombreProyecto() {
		return nombreProyecto;
	}
	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}
	public String getDescripcionProyecto() {
		return descripcionProyecto;
	}
	public void setDescripcionProyecto(String descripcionProyecto) {
		this.descripcionProyecto = descripcionProyecto;
	}
	public byte[] getFotoProyecto() {
		return fotoProyecto;
	}
	public void setFotoProyecto(byte[] fotoProyecto) {
		this.fotoProyecto = fotoProyecto;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
}
