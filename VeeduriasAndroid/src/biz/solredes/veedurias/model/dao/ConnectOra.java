package biz.solredes.veedurias.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;

import biz.solredes.veedurias.controller.InidiraVeeduriaMain;

/**
 * Created by Henry on 25/09/2014.
 */
public class ConnectOra { public String driver, url, ip, bd, usr, pass;
    public Connection conexion;

    public ConnectOra(String ip, String bd, String usr, String pass) {
        driver = "oracle.jdbc.driver.OracleDriver";

        this.bd = bd;
        this.usr = usr;
        this.pass = pass;

        url = new String("jdbc:oracle:thin:@"+ ip +":1521:"+bd);

        try {
            Locale.setDefault(new Locale("es", "ES"));
        	Class.forName(driver).newInstance();
            
            conexion = DriverManager.getConnection(url, usr, pass);
            // revisan el logcat para ver el mensaje
            System.out.println("Conexion a Base de Datos " + bd + " Exitosa");

        } catch (SQLException exc) {
    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
    		t.salir();
            System.out.println("Error al tratar de abrir la base de Datos "
                    + bd + " : " + exc);
        } catch (ClassNotFoundException ex){
            System.out.println("Error classnotfound "
                    + bd + " : " + ex);
    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
    		t.salir();
        } catch (InstantiationException ex){
            System.out.println("Error instantation "
                    + bd + " : " + ex);
    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
    		t.salir();
        } catch (IllegalAccessException ex) {
            System.out.println("Error illegal "
                    + bd + " : " + ex);
    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
    		t.salir();
        }
    }

    public Connection getConexion() {
        return conexion;
    }

    public void CerrarConexion() throws SQLException {
        conexion.close();
    }
}