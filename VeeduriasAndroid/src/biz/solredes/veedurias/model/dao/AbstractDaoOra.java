package biz.solredes.veedurias.model.dao;


import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import biz.solredes.veedurias.controller.InidiraVeeduriaMain;
import android.os.StrictMode;


public class AbstractDaoOra  {
	public String nombre,descripcion,estado,codigocontrato,duracion,fechainicio,fechafin,valor,avancefinanciero,avancefisico,localizacion,financiacion,pais,departamento,municipio,lugar,indicadores,secretaria,dependencia,meta;
	public float calificacion;
	public InputStream foto;
	private String ipdarien2="192.168.0.45",usrdarien2="C##DARIEN2015_INIRIDA",servdarien2="DBMASTER",pswdarien2="DARIEN2015_INIRIDA";
	private String ipsico="192.168.0.45",usrsico="C##SICO2015_INIRIDA",servsico="DBMASTER",pswsico="SICO2015_INIRIDA";


	
	
	public String getMeta() {
		return meta;
	}
	public void setMeta(String meta) {
		this.meta = meta;
	}
	public AbstractDaoOra()
	{
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().build();
        StrictMode.setThreadPolicy(policy);
        
	}
	 public ArrayList<String> getDescripcionbusqueda(){
	    	ArrayList<String> nombreProyecto = new ArrayList<String>();
	    	String sql = "SELECT PRY_OBJETO AS descripciones FROM S_PROYECTO";
	    	try{
	    		Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();
	    		ResultSet resultados = sentencia.executeQuery(sql);
	    		
	    		while(resultados.next()){
	    			nombreProyecto.add(resultados.getString("descripciones"));

	    		}
	    	} catch (SQLException ex) {
			            	
	    	}
	    	
	    	return nombreProyecto;
	    }
	    public ArrayList<String> getDescripcioncategoria(String Secretaria){
	    	ArrayList<String> nombreProyecto = new ArrayList<String>();
	    	String sql = "SELECT CAT_NOMBRE AS descripciones FROM B_CATEGORIA BC WHERE BC.DEP_ID=(SELECT BD.DEP_ID  FROM C##SICO2015_INIRIDA.B_DEPENDENCIA BD WHERE BD.DEP_NOMBRE ='"+ Secretaria+"'  ) ";
	    	try{
	    		Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();
	    		ResultSet resultados = sentencia.executeQuery(sql);
	    		while(resultados.next()){
	    			nombreProyecto.add(resultados.getString("descripciones"));
	    		}
	    	} catch (SQLException ex) {
	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();

	    	}
	    	
	    	return nombreProyecto;
	    }
	    public ArrayList<String> getDescripcionfiltro(String categoria){
	    	ArrayList<String> nombreProyecto = new ArrayList<String>();
	    	String sql = "SELECT PRY_OBJETO AS descripciones FROM S_PROYECTO BC WHERE BC.CAT_ID =(SELECT BD.CAT_ID  FROM B_CATEGORIA BD WHERE BD.CAT_NOMBRE ='"+categoria+"')";
	    	try{
	    		Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();
	    		ResultSet resultados = sentencia.executeQuery(sql);
	    		while(resultados.next()){
	    			nombreProyecto.add(resultados.getString("descripciones"));
	    		}
	    	} catch (SQLException ex) {
	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();
	    	}
	    	
	    	return nombreProyecto;
	    }
	    public ArrayList<String> getDescripcionfiltroespecial(String Secretaria, String Filtro){
	    	ArrayList<String> nombreProyecto = new ArrayList<String>();
	    	String sql = "SELECT SP.PRY_OBJETO AS descripciones FROM S_PROYECTO SP, B_CATEGORIA BC, C##SICO2015_INIRIDA.B_DEPENDENCIA BD WHERE BD.DEP_ID = BC.DEP_ID AND SP.CAT_ID = BC.CAT_ID AND BD.DEP_NOMBRE = '"+Secretaria+"' ORDER BY "+Filtro+"";
	    	try{
	    		Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();
	    		ResultSet resultados = sentencia.executeQuery(sql);
	    		while(resultados.next()){
	    			nombreProyecto.add(resultados.getString("descripciones"));
	    		}
	    	} catch (SQLException ex) {
	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();

            	}
	    	
	    	return nombreProyecto;
	    }

	    public ArrayList<String> getDescripcionsecretarias(){
	    	ArrayList<String> nombreProyecto = new ArrayList<String>();
	    	String sql = "SELECT DEP_NOMBRE AS descripcion FROM  C##SICO2015_INIRIDA.B_DEPENDENCIA WHERE DEP_MADRE=7 AND DEP_NUM_EXTERNA=0";
	    	try{
	    		Statement sentencia = new ConnectOra(ipsico, servsico, usrsico, pswsico).getConexion().createStatement();
	    		ResultSet resultados = sentencia.executeQuery(sql);
	    		while(resultados.next()){
	    			nombreProyecto.add(resultados.getString("descripcion"));
	    		}
	    	} catch (SQLException ex) {
	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();

	    	}
	    	
	    	return nombreProyecto;
	    }
	    public ArrayList<String> getDescripcioncomentarios(String proyecto){
	    	ArrayList<String> nombreProyecto = new ArrayList<String>();
	    
	    	String sql
	    	="SELECT com.PRCOM_COMENTARIO AS descripcion FROM S_PRY_COMENTARIOS com,  S_PROYECTO S WHERE  COM.PRY_ID = S.PRY_ID AND S.PRY_OBJETO = '"+proyecto+"'";
	    	try{
	    		Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();
	    		ResultSet resultados = sentencia.executeQuery(sql);
	    		while(resultados.next()){
	    			nombreProyecto.add(resultados.getString("descripcion"));
	    			
	    		}
	    	} catch (SQLException ex) {

	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();

	    	}
	    	System.out.println(nombreProyecto);
	    	return nombreProyecto;
	    }
	    
	  
	    public void proyectodata(String nombres)
	    {

			String sql ="SELECT  PRY_FOTO AS foto, " +
					"SP.PRY_OBJETO AS nombre," +
					"SP.PRY_DESCRIPCION AS descripcion, " +
					"SP.PRY_COD_CONTRATO AS codigocontrato, " +
					"SP.PRY_DURACION AS duracion, " +
					"to_char(SP.PRY_FECHA_INI , 'YYYY-MM-dd') AS fechainicio, " +
					"to_char(SP.PRY_FECHA_FIN , 'YYYY-MM-dd')AS fechafin,  " +
					"to_char(SP.PRY_VALOR, '$999,999,999,999.99')AS valor, " +
					"SP.PRY_AVANCE_FINANCIERO AS avancefinanciero, " +
					"SP.PRY_AVANCE_FISICO AS avancefisico, " +
					"SP.PRY_LUGAR AS lugar, " +
					"SP.PRY_IND AS indicador, " +
					"SP.PRY_PROMEDIO_CAL AS calificacion, " +
					"SP.PRY_META AS meta,"+
					"CP.CAT_NOMBRE AS categoria, " +
					"(SELECT TP.BAT_NOMBRE FROM C##SICO2015_INIRIDA.B_BASICA_TIPO TP WHERE TP.BAT_ID=SP.EST_ID ) AS ESTADO , " +
					"(SELECT TP.BAT_NOMBRE FROM C##SICO2015_INIRIDA.B_BASICA_TIPO TP WHERE TP.BAT_ID=SP.LOC_ID ) AS LOCALIZACION , " +
					"(SELECT TP.BAT_NOMBRE FROM C##SICO2015_INIRIDA.B_BASICA_TIPO TP WHERE TP.BAT_ID=SP.FIN_ID ) AS FINANCIACION, " +
					"(SELECT TP.BAT_NOMBRE FROM C##SICO2015_INIRIDA.B_BASICA_TIPO TP WHERE TP.BAT_ID=SP.PAIS_ID )  AS PAIS , " +
					"(SELECT TP.BAT_NOMBRE FROM C##SICO2015_INIRIDA.B_BASICA_TIPO TP WHERE TP.BAT_ID=SP.DEPTO_ID ) AS DEPARTAMENTO, " +
					"(SELECT TR.DEP_NOMBRE FROM C##SICO2015_INIRIDA.B_DEPENDENCIA TR WHERE TR.DEP_ID=SP.DEP_ID ) AS dependencia, " +
					"(SELECT TP.BAT_NOMBRE FROM C##SICO2015_INIRIDA.B_BASICA_TIPO TP WHERE TP.BAT_ID=SP.MUN_ID ) AS MUNICIPIO " +
					"FROM  S_PROYECTO SP, C##SICO2015_INIRIDA.B_BASICA_TIPO BT,C##SICO2015_INIRIDA.B_DEPENDENCIA BD, C##SICO2015_INIRIDA.B_BASICA_TABLA BTL,B_CATEGORIA CP " +
					"WHERE  " +
					"SP.CAT_ID= CP.CAT_ID " +
					"AND CP.DEP_ID = BD.DEP_ID " +
					"AND SP.CAT_ID = BT.BAT_ID " +
					"AND BT.BAT_ID = BTL.BTL_ID " +
					"AND SP.PRY_OBJETO ='"+nombres+"'";

					try {
				Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();

				ResultSet rs = sentencia.executeQuery(sql);

				while (rs.next()) {
					setNombre(rs.getString("nombre"));
					setDescripcion(rs.getString("descripcion"));
				    setEstado(rs.getString("ESTADO"));
				    setCodigocontrato(rs.getString("codigocontrato"));
				    setDuracion(rs.getString("duracion"));
				    setFechainicio(rs.getString("fechainicio"));
				    setFechafin(rs.getString("fechafin"));
				    setValor(rs.getString("valor"));
				    setAvancefinanciero(rs.getString("avancefinanciero"));
				    setAvancefisico(rs.getString("avancefisico"));
				    setLocalizacion(rs.getString("LOCALIZACION"));
				    setFinanciacion(rs.getString("FINANCIACION"));
				    setPais(rs.getString("PAIS"));
				    setDepartamento(rs.getString("DEPARTAMENTO"));
				    setMunicipio(rs.getString("MUNICIPIO"));
				    setFoto(rs.getBinaryStream("foto"));
				    setLugar(rs.getString("lugar"));
				    setIndicadores(rs.getString("indicador"));
				    setCalificacion(rs.getFloat("calificacion"));
				    setDependencia(rs.getString("dependencia"));
				    setSecretaria(rs.getString("categoria"));
				    setMeta(rs.getString("meta"));
				   
				}
			} catch (SQLException e) {

	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();
			}
	    }
	    public  void detalledata(String proyecto, String comentario)
	    {
	    	String sql= "SELECT com.PRCOM_COMENTARIO AS descripcion, COM.PRCOM_CALIFICACION AS calificacion, COM.PRCOM_NOMBRE as nombre, COM.PRCOM_FOTO AS foto  FROM S_PRY_COMENTARIOS com,  S_PROYECTO S WHERE  COM.PRY_ID = S.PRY_ID AND S.PRY_OBJETO = '"+proyecto+"' and COM.PRCOM_COMENTARIO='"+comentario+"'" ;
	    	try {
				Statement sentencia = new ConnectOra(ipdarien2, servdarien2, usrdarien2, pswdarien2).getConexion().createStatement();
				ResultSet rs= sentencia.executeQuery(sql);
				while(rs.next())
				{
					setNombre(rs.getString("nombre"));
					setDescripcion(rs.getString("descripcion"));
					setCalificacion(rs.getFloat("calificacion"));
					setFoto(rs.getBinaryStream("foto"));
				}
	    	
	    	} catch (SQLException e) {
	    		InidiraVeeduriaMain t = new InidiraVeeduriaMain();
	    		t.salir();
			}
	    }

	    
		public String getSecretaria() {
			return secretaria;
		}
		public void setSecretaria(String secretaria) {
			this.secretaria = secretaria;
		}
		public String getDependencia() {
			return dependencia;
		}
		public void setDependencia(String dependencia) {
			this.dependencia = dependencia;
		}
		public String getLugar() {
			return lugar;
		}
		public void setLugar(String lugar) {
			this.lugar = lugar;
		}
		public String getIndicadores() {
			return indicadores;
		}
		public void setIndicadores(String indicadores) {
			this.indicadores = indicadores;
		}
		public float getCalificacion() {
			return calificacion;
		}
		public void setCalificacion(float calificacion) {
			this.calificacion = calificacion;
		}
		public InputStream getFoto() {
			return foto;
		}
		public void setFoto(InputStream foto) {
			this.foto = foto;
		}
		public String getPais() {
			return pais;
		}
		public void setPais(String pais) {
			this.pais = pais;
		}
		public String getDepartamento() {
			return departamento;
		}
		public void setDepartamento(String departamento) {
			this.departamento = departamento;
		}
		public String getMunicipio() {
			return municipio;
		}
		public void setMunicipio(String municipio) {
			this.municipio = municipio;
		}
		public String getFinanciacion() {
			return financiacion;
		}
		public void setFinanciacion(String financiacion) {
			this.financiacion = financiacion;
		}
		public String getLocalizacion() {
			return localizacion;
		}
		public void setLocalizacion(String localizacion) {
			this.localizacion = localizacion;
		}
		public String getAvancefisico() {
			return avancefisico;
		}
		public void setAvancefisico(String avancefisico) {
			this.avancefisico = avancefisico;
		}
		public String getAvancefinanciero() {
			return avancefinanciero;
		}
		public void setAvancefinanciero(String avancefinanciero) {
			this.avancefinanciero = avancefinanciero;
		}
		public String getValor() {
			return valor;
		}
		public void setValor(String valor) {
			this.valor = valor;
		}
		public String getFechafin() {
			return fechafin;
		}
		public void setFechafin(String fechafin) {
			this.fechafin = fechafin;
		}
		public String getFechainicio() {
			return fechainicio;
		}
		public void setFechainicio(String fechainicio) {
			this.fechainicio = fechainicio;
		}
		public String getDuracion() {
			return duracion;
		}
		public void setDuracion(String duracion) {
			this.duracion = duracion;
		}
		public String getCodigocontrato() {
			return codigocontrato;
		}
		public void setCodigocontrato(String codigocontrato) {
			this.codigocontrato = codigocontrato;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public String getEstado() {
			return estado;
		}
		public void setEstado(String estado) {
			this.estado = estado;
		}


		

}
