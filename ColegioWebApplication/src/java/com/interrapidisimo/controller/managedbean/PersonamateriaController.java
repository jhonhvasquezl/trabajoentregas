package com.interrapidisimo.controller.managedbean;

import com.interrapidisimo.model.entities.Personamateria;
import com.interrapidisimo.controller.managedbean.util.JsfUtil;
import com.interrapidisimo.controller.managedbean.util.JsfUtil.PersistAction;
import com.interrapidisimo.model.facade.PersonamateriaFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("personamateriaController")
@SessionScoped
public class PersonamateriaController implements Serializable {

    @EJB
    private com.interrapidisimo.model.facade.PersonamateriaFacade ejbFacade;
    private List<Personamateria> items = null;
    private Personamateria selected;

    public PersonamateriaController() {
    }

    public Personamateria getSelected() {
        return selected;
    }

    public void setSelected(Personamateria selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PersonamateriaFacade getFacade() {
        return ejbFacade;
    }

    public Personamateria prepareCreate() {
        selected = new Personamateria();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        if (validarUsiario(selected.getPersonapersonaid().getPersonaId(),selected.getPersonapersonaid().getTipoPersonatipopersonaid().getTipopersonaId())) {
            persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("PersonamateriaCreated"));
            if (!JsfUtil.isValidationFailed()) {
                items = null;    // Invalidate list of items to trigger re-query.
            }
        }
        else{ JsfUtil.addErrorMessage("El usuario: "+ selected.getPersonapersonaid().getPersonaNombre()+" " + selected.getPersonapersonaid().getPersonaApellido()+" ha excedido el numero de creditos inscritos");}
    }
    

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("PersonamateriaUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("PersonamateriaDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Personamateria> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Personamateria getPersonamateria(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Personamateria> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Personamateria> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }
    
    private boolean validarUsiario(int id, int tipoUsuario)
    {
        List<Personamateria> valores=ejbFacade.validarIngreso(id);
        if(tipoUsuario==1 && valores.size()>2){
            return false;
        }
        if(tipoUsuario==2 && valores.size()>1){
            return false;
        }
        return true;      
    }

    @FacesConverter(forClass = Personamateria.class)
    public static class PersonamateriaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PersonamateriaController controller = (PersonamateriaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "personamateriaController");
            return controller.getPersonamateria(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Personamateria) {
                Personamateria o = (Personamateria) object;
                return getStringKey(o.getPersonamateriaId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Personamateria.class.getName()});
                return null;
            }
        }

    }

}
