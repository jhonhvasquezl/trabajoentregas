/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Henry
 */
@Entity
@Table(name = "persona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p"),
    @NamedQuery(name = "Persona.findByPersonaId", query = "SELECT p FROM Persona p WHERE p.personaId = :personaId"),
    @NamedQuery(name = "Persona.findByPersonaNombre", query = "SELECT p FROM Persona p WHERE p.personaNombre = :personaNombre"),
    @NamedQuery(name = "Persona.findByPersonaApellido", query = "SELECT p FROM Persona p WHERE p.personaApellido = :personaApellido"),
    @NamedQuery(name = "Persona.findByPersonaTelefono", query = "SELECT p FROM Persona p WHERE p.personaTelefono = :personaTelefono"),
    @NamedQuery(name = "Persona.findByPersonaDocumento", query = "SELECT p FROM Persona p WHERE p.personaDocumento = :personaDocumento")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "persona_id")
    private Integer personaId;
    @Size(max = 120)
    @Column(name = "persona_nombre")
    private String personaNombre;
    @Size(max = 120)
    @Column(name = "persona_apellido")
    private String personaApellido;
    @Size(max = 12)
    @Column(name = "persona_telefono")
    private String personaTelefono;
    @Size(max = 30)
    @Column(name = "persona_documento")
    private String personaDocumento;
    @JoinColumn(name = "TipoDocumnto_tipodocumnto_id", referencedColumnName = "tipodocumnto_id")
    @ManyToOne(optional = false)
    private Tipodocumnto tipoDocumntotipodocumntoid;
    @JoinColumn(name = "TipoPersona_tipopersona_id", referencedColumnName = "tipopersona_id")
    @ManyToOne(optional = false)
    private Tipopersona tipoPersonatipopersonaid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personapersonaid")
    private Collection<Personamateria> personamateriaCollection;

    public Persona() {
    }

    public Persona(Integer personaId) {
        this.personaId = personaId;
    }

    public Integer getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Integer personaId) {
        this.personaId = personaId;
    }

    public String getPersonaNombre() {
        return personaNombre;
    }

    public void setPersonaNombre(String personaNombre) {
        this.personaNombre = personaNombre;
    }

    public String getPersonaApellido() {
        return personaApellido;
    }

    public void setPersonaApellido(String personaApellido) {
        this.personaApellido = personaApellido;
    }

    public String getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(String personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public String getPersonaDocumento() {
        return personaDocumento;
    }

    public void setPersonaDocumento(String personaDocumento) {
        this.personaDocumento = personaDocumento;
    }

    public Tipodocumnto getTipoDocumntotipodocumntoid() {
        return tipoDocumntotipodocumntoid;
    }

    public void setTipoDocumntotipodocumntoid(Tipodocumnto tipoDocumntotipodocumntoid) {
        this.tipoDocumntotipodocumntoid = tipoDocumntotipodocumntoid;
    }

    public Tipopersona getTipoPersonatipopersonaid() {
        return tipoPersonatipopersonaid;
    }

    public void setTipoPersonatipopersonaid(Tipopersona tipoPersonatipopersonaid) {
        this.tipoPersonatipopersonaid = tipoPersonatipopersonaid;
    }

    @XmlTransient
    public Collection<Personamateria> getPersonamateriaCollection() {
        return personamateriaCollection;
    }

    public void setPersonamateriaCollection(Collection<Personamateria> personamateriaCollection) {
        this.personamateriaCollection = personamateriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personaId != null ? personaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.personaId == null && other.personaId != null) || (this.personaId != null && !this.personaId.equals(other.personaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Rol: "+tipoPersonatipopersonaid.getTipopersonaNombre()+" Nombre: "+personaNombre+" "+personaApellido ;
    }
    
}
