/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Henry
 */
@Entity
@Table(name = "materia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materia.findAll", query = "SELECT m FROM Materia m"),
    @NamedQuery(name = "Materia.findByMateriaId", query = "SELECT m FROM Materia m WHERE m.materiaId = :materiaId"),
    @NamedQuery(name = "Materia.findByMateriaNombre", query = "SELECT m FROM Materia m WHERE m.materiaNombre = :materiaNombre"),
    @NamedQuery(name = "Materia.findByMateriaCreditos", query = "SELECT m FROM Materia m WHERE m.materiaCreditos = :materiaCreditos")})
public class Materia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "materia_id")
    private Integer materiaId;
    @Size(max = 100)
    @Column(name = "materia_nombre")
    private String materiaNombre;
    @Column(name = "materia_creditos")
    private Integer materiaCreditos;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "materiamateriaid")
    private Collection<Personamateria> personamateriaCollection;

    public Materia() {
    }

    public Materia(Integer materiaId) {
        this.materiaId = materiaId;
    }

    public Integer getMateriaId() {
        return materiaId;
    }

    public void setMateriaId(Integer materiaId) {
        this.materiaId = materiaId;
    }

    public String getMateriaNombre() {
        return materiaNombre;
    }

    public void setMateriaNombre(String materiaNombre) {
        this.materiaNombre = materiaNombre;
    }

    public Integer getMateriaCreditos() {
        return materiaCreditos;
    }

    public void setMateriaCreditos(Integer materiaCreditos) {
        this.materiaCreditos = materiaCreditos;
    }

    @XmlTransient
    public Collection<Personamateria> getPersonamateriaCollection() {
        return personamateriaCollection;
    }

    public void setPersonamateriaCollection(Collection<Personamateria> personamateriaCollection) {
        this.personamateriaCollection = personamateriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (materiaId != null ? materiaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materia)) {
            return false;
        }
        Materia other = (Materia) object;
        if ((this.materiaId == null && other.materiaId != null) || (this.materiaId != null && !this.materiaId.equals(other.materiaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return materiaNombre;
    }
    
}
