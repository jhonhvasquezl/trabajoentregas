/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Henry
 */
@Entity
@Table(name = "tipodocumnto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipodocumnto.findAll", query = "SELECT t FROM Tipodocumnto t"),
    @NamedQuery(name = "Tipodocumnto.findByTipodocumntoId", query = "SELECT t FROM Tipodocumnto t WHERE t.tipodocumntoId = :tipodocumntoId"),
    @NamedQuery(name = "Tipodocumnto.findByTipodocumntoNombre", query = "SELECT t FROM Tipodocumnto t WHERE t.tipodocumntoNombre = :tipodocumntoNombre")})
public class Tipodocumnto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipodocumnto_id")
    private Integer tipodocumntoId;
    @Size(max = 100)
    @Column(name = "tipodocumnto_nombre")
    private String tipodocumntoNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoDocumntotipodocumntoid")
    private Collection<Persona> personaCollection;

    public Tipodocumnto() {
    }

    public Tipodocumnto(Integer tipodocumntoId) {
        this.tipodocumntoId = tipodocumntoId;
    }

    public Integer getTipodocumntoId() {
        return tipodocumntoId;
    }

    public void setTipodocumntoId(Integer tipodocumntoId) {
        this.tipodocumntoId = tipodocumntoId;
    }

    public String getTipodocumntoNombre() {
        return tipodocumntoNombre;
    }

    public void setTipodocumntoNombre(String tipodocumntoNombre) {
        this.tipodocumntoNombre = tipodocumntoNombre;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipodocumntoId != null ? tipodocumntoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipodocumnto)) {
            return false;
        }
        Tipodocumnto other = (Tipodocumnto) object;
        if ((this.tipodocumntoId == null && other.tipodocumntoId != null) || (this.tipodocumntoId != null && !this.tipodocumntoId.equals(other.tipodocumntoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tipodocumntoNombre +"";
    }
    
}
