/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Henry
 */
@Entity
@Table(name = "personamateria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personamateria.findAll", query = "SELECT p FROM Personamateria p"),
    @NamedQuery(name = "Personamateria.findByPersonamateriaId", query = "SELECT p FROM Personamateria p WHERE p.personamateriaId = :personamateriaId")})
public class Personamateria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "personamateria_id")
    private Integer personamateriaId;
    @JoinColumn(name = "Materia_materia_id", referencedColumnName = "materia_id")
    @ManyToOne(optional = false)
    private Materia materiamateriaid;
    @JoinColumn(name = "Persona_persona_id", referencedColumnName = "persona_id")
    @ManyToOne(optional = false)
    private Persona personapersonaid;

    public Personamateria() {
    }

    public Personamateria(Integer personamateriaId) {
        this.personamateriaId = personamateriaId;
    }

    public Integer getPersonamateriaId() {
        return personamateriaId;
    }

    public void setPersonamateriaId(Integer personamateriaId) {
        this.personamateriaId = personamateriaId;
    }

    public Materia getMateriamateriaid() {
        return materiamateriaid;
    }

    public void setMateriamateriaid(Materia materiamateriaid) {
        this.materiamateriaid = materiamateriaid;
    }

    public Persona getPersonapersonaid() {
        return personapersonaid;
    }

    public void setPersonapersonaid(Persona personapersonaid) {
        this.personapersonaid = personapersonaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personamateriaId != null ? personamateriaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personamateria)) {
            return false;
        }
        Personamateria other = (Personamateria) object;
        if ((this.personamateriaId == null && other.personamateriaId != null) || (this.personamateriaId != null && !this.personamateriaId.equals(other.personamateriaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.interrapidisimo.model.entities.Personamateria[ personamateriaId=" + personamateriaId + " ]";
    }
    
}
