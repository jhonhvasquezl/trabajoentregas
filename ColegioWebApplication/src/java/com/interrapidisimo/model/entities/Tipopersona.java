/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Henry
 */
@Entity
@Table(name = "tipopersona")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipopersona.findAll", query = "SELECT t FROM Tipopersona t"),
    @NamedQuery(name = "Tipopersona.findByTipopersonaId", query = "SELECT t FROM Tipopersona t WHERE t.tipopersonaId = :tipopersonaId"),
    @NamedQuery(name = "Tipopersona.findByTipopersonaNombre", query = "SELECT t FROM Tipopersona t WHERE t.tipopersonaNombre = :tipopersonaNombre")})
public class Tipopersona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipopersona_id")
    private Integer tipopersonaId;
    @Size(max = 100)
    @Column(name = "tipopersona_nombre")
    private String tipopersonaNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoPersonatipopersonaid")
    private Collection<Persona> personaCollection;

    public Tipopersona() {
    }

    public Tipopersona(Integer tipopersonaId) {
        this.tipopersonaId = tipopersonaId;
    }

    public Integer getTipopersonaId() {
        return tipopersonaId;
    }

    public void setTipopersonaId(Integer tipopersonaId) {
        this.tipopersonaId = tipopersonaId;
    }

    public String getTipopersonaNombre() {
        return tipopersonaNombre;
    }

    public void setTipopersonaNombre(String tipopersonaNombre) {
        this.tipopersonaNombre = tipopersonaNombre;
    }

    @XmlTransient
    public Collection<Persona> getPersonaCollection() {
        return personaCollection;
    }

    public void setPersonaCollection(Collection<Persona> personaCollection) {
        this.personaCollection = personaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipopersonaId != null ? tipopersonaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipopersona)) {
            return false;
        }
        Tipopersona other = (Tipopersona) object;
        if ((this.tipopersonaId == null && other.tipopersonaId != null) || (this.tipopersonaId != null && !this.tipopersonaId.equals(other.tipopersonaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tipopersonaNombre +"";
    }
    
}
