/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.facade;

import com.interrapidisimo.model.entities.Personamateria;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Henry
 */
@Stateless
public class PersonamateriaFacade extends AbstractFacade<Personamateria> {

    @PersistenceContext(unitName = "ColegioWebApplicationPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonamateriaFacade() {
        super(Personamateria.class);
    }
    public List<Personamateria> validarIngreso(int idUsuario){
        Query q = getEntityManager().createQuery("SELECT a FROM Personamateria a WHERE a.personapersonaid.personaId = :idUsu");
        q.setParameter("idUsu", idUsuario);
        return q.getResultList();
    }

}
