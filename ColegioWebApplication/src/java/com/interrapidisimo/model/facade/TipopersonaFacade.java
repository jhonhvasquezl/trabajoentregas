/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.interrapidisimo.model.facade;

import com.interrapidisimo.model.entities.Tipopersona;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Henry
 */
@Stateless
public class TipopersonaFacade extends AbstractFacade<Tipopersona> {

    @PersistenceContext(unitName = "ColegioWebApplicationPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipopersonaFacade() {
        super(Tipopersona.class);
    }
    
}
