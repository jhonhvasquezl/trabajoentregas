-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2020 at 05:09 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `colegiodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `materia`
--

CREATE TABLE `materia` (
  `materia_id` int(11) NOT NULL,
  `materia_nombre` varchar(100) DEFAULT NULL,
  `materia_creditos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materia`
--

INSERT INTO `materia` (`materia_id`, `materia_nombre`, `materia_creditos`) VALUES
(1, 'Matematicas', 3),
(2, 'Español', 3),
(3, 'Ingles', 3),
(4, 'Sociales', 3),
(5, 'Calculo', 3),
(6, 'Introducción algoritmos', 3),
(7, 'Algebra', 3),
(8, 'Biologia', 3),
(9, 'Quimica', 3),
(10, 'Historia', 3),
(11, 'Fisica', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE `persona` (
  `persona_id` int(11) NOT NULL,
  `persona_nombre` varchar(120) DEFAULT NULL,
  `persona_apellido` varchar(120) DEFAULT NULL,
  `persona_telefono` varchar(12) DEFAULT NULL,
  `TipoDocumnto_tipodocumnto_id` int(11) NOT NULL,
  `persona_documento` varchar(30) DEFAULT NULL,
  `TipoPersona_tipopersona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`persona_id`, `persona_nombre`, `persona_apellido`, `persona_telefono`, `TipoDocumnto_tipodocumnto_id`, `persona_documento`, `TipoPersona_tipopersona_id`) VALUES
(1, 'Jose', 'Canseco', '654646', 1, '98798464', 1),
(2, 'Andrea', 'Cardenas', '6545646', 1, '3216456464', 1),
(3, 'Andres', 'Fonseca', '5345353', 2, '123131313', 2),
(4, 'Henry', 'Vasquez', '2343243', 2, '142432342', 2),
(5, 'Juan', 'Galindo', '53453453', 2, '2323424222', 2),
(6, 'Rosalinda', 'Sanchez', '23424242', 2, '45984595498', 2),
(7, 'Maria', 'Leon', '1221323', 2, '32443253', 2),
(8, 'Alvaro', 'Vasquez', '234234', 2, '243242424', 1),
(9, 'Daniel', 'Cruz', '1212323', 2, '567657567', 1),
(10, 'Alberto', 'Rojas', '213123', 2, '23432424', 1),
(11, 'Joaquin', 'Calderon', '2342345', 1, '7987987965', 1),
(12, 'John', 'Ramirez', '3215469', 2, '5463126457', 2);

-- --------------------------------------------------------

--
-- Table structure for table `personamateria`
--

CREATE TABLE `personamateria` (
  `personamateria_id` int(11) NOT NULL,
  `Persona_persona_id` int(11) NOT NULL,
  `Materia_materia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personamateria`
--

INSERT INTO `personamateria` (`personamateria_id`, `Persona_persona_id`, `Materia_materia_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 4),
(6, 3, 1),
(7, 3, 5),
(8, 4, 2),
(9, 4, 3),
(10, 5, 4),
(11, 5, 6),
(12, 6, 7),
(13, 6, 8),
(14, 7, 9),
(15, 7, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tipodocumnto`
--

CREATE TABLE `tipodocumnto` (
  `tipodocumnto_id` int(11) NOT NULL,
  `tipodocumnto_nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipodocumnto`
--

INSERT INTO `tipodocumnto` (`tipodocumnto_id`, `tipodocumnto_nombre`) VALUES
(1, 'Tarjeta de identidad'),
(2, 'Cedula de ciudadanía'),
(3, 'Cedula de extranjería');

-- --------------------------------------------------------

--
-- Table structure for table `tipopersona`
--

CREATE TABLE `tipopersona` (
  `tipopersona_id` int(11) NOT NULL,
  `tipopersona_nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tipopersona`
--

INSERT INTO `tipopersona` (`tipopersona_id`, `tipopersona_nombre`) VALUES
(1, 'Estudiante'),
(2, 'Profesor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`materia_id`);

--
-- Indexes for table `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`persona_id`),
  ADD KEY `fk_Persona_TipoPersona1` (`TipoPersona_tipopersona_id`),
  ADD KEY `fk_Persona_TipoDocumnto1` (`TipoDocumnto_tipodocumnto_id`);

--
-- Indexes for table `personamateria`
--
ALTER TABLE `personamateria`
  ADD PRIMARY KEY (`personamateria_id`),
  ADD KEY `fk_PersonaMateria_Persona` (`Persona_persona_id`),
  ADD KEY `fk_PersonaMateria_Materia1` (`Materia_materia_id`);

--
-- Indexes for table `tipodocumnto`
--
ALTER TABLE `tipodocumnto`
  ADD PRIMARY KEY (`tipodocumnto_id`);

--
-- Indexes for table `tipopersona`
--
ALTER TABLE `tipopersona`
  ADD PRIMARY KEY (`tipopersona_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `materia`
--
ALTER TABLE `materia`
  MODIFY `materia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `persona`
--
ALTER TABLE `persona`
  MODIFY `persona_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `personamateria`
--
ALTER TABLE `personamateria`
  MODIFY `personamateria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tipodocumnto`
--
ALTER TABLE `tipodocumnto`
  MODIFY `tipodocumnto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipopersona`
--
ALTER TABLE `tipopersona`
  MODIFY `tipopersona_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `fk_Persona_TipoDocumnto1` FOREIGN KEY (`TipoDocumnto_tipodocumnto_id`) REFERENCES `tipodocumnto` (`tipodocumnto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_TipoPersona1` FOREIGN KEY (`TipoPersona_tipopersona_id`) REFERENCES `tipopersona` (`tipopersona_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `personamateria`
--
ALTER TABLE `personamateria`
  ADD CONSTRAINT `fk_PersonaMateria_Materia1` FOREIGN KEY (`Materia_materia_id`) REFERENCES `materia` (`materia_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PersonaMateria_Persona` FOREIGN KEY (`Persona_persona_id`) REFERENCES `persona` (`persona_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
