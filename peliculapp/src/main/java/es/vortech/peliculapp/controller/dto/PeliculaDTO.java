package es.vortech.peliculapp.controller.dto;


public class PeliculaDTO {

	 private Long id;
	 private String titulo;
	 private String genero;
	 private int nosscar;
	 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getNosscar() {
		return nosscar;
	}
	public void setNosscar(int nosscar) {
		this.nosscar = nosscar;
	}
	 
}
