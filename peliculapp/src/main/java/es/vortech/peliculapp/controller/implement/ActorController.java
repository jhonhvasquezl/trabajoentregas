package es.vortech.peliculapp.controller.implement;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.vortech.peliculapp.controller.IActorController;
import es.vortech.peliculapp.controller.dto.ActorDTO;
import es.vortech.peliculapp.service.IActorService;
import es.vortech.peliculapp.util.ActorUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Servicio de Actor", description = "Esta api tiene las operaciones sobre la entidad actor")
public class ActorController implements IActorController  {

	@Autowired
	IActorService actorService;

	
	@RequestMapping(value = "/actor", method = RequestMethod.GET, produces = "application/json")	
    @ApiOperation(value = "Listado de actores", notes = "Trae una lista completa de actores")
	@Override
	public List<ActorDTO> getActors() {
		// TODO Auto-generated method stub
		return ActorUtil.dtoToModel(actorService.getActors());
	}

	@Override
	@RequestMapping(value = "/actor/add", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Guardado actor", notes = "metodo encargado de salvar a un actor en la base de datos")
	public ActorDTO saveActor(@RequestBody ActorDTO dto) {
		// TODO Auto-generated method stub
		return ActorUtil.objDtoToModel(actorService.saveActor(ActorUtil.objModelToDto(dto)));
	}

	@Override
	@RequestMapping(value = "/actor/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "actor por id", notes = "Trae un actor por id")
	public ActorDTO getActorById(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return ActorUtil.objDtoToModel(actorService.getActorById(id));
	}

	@RequestMapping(value = "/actors/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Listado de actores por id", notes = "Trae una lista completa de actores por id")
	@Override
	public List<ActorDTO> getActorsById(@PathVariable Long id) {
		// TODO Auto-generated method stub
	    ArrayList<Long> miLista = new ArrayList<Long>();
		miLista.add(id);
		return ActorUtil.dtoToModel(actorService.getActorsById(miLista));
	}
}
