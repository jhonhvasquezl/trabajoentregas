package es.vortech.peliculapp.controller;

import java.util.List;

import es.vortech.peliculapp.controller.dto.ActorPeliculaDTO;

public interface IActorPeliculaController {

	List<ActorPeliculaDTO> getActorPeliculas();
	ActorPeliculaDTO saveActorPelicula(ActorPeliculaDTO dto);
	ActorPeliculaDTO getActorPeliculaById(Long id);
	List<ActorPeliculaDTO> getActorPeliculasById(Long id);
}
