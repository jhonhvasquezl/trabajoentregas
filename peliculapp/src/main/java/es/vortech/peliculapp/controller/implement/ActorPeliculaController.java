package es.vortech.peliculapp.controller.implement;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.vortech.peliculapp.controller.IActorPeliculaController;
import es.vortech.peliculapp.controller.dto.ActorPeliculaDTO;
import es.vortech.peliculapp.service.IActorPeliculaService;
import es.vortech.peliculapp.util.ActorPeliculaUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Servicio de ActorPelicula", description = "Esta api tiene las operaciones sobre la entidad actorPelicula")
public class ActorPeliculaController implements IActorPeliculaController{

	@Autowired
	IActorPeliculaService actorPeliculaService;

	
	@RequestMapping(value = "/actorPelicula", method = RequestMethod.GET, produces = "application/json")	
    @ApiOperation(value = "Listado de actorPeliculas", notes = "Trae una lista completa de actorPeliculas")
	@Override
	public List<ActorPeliculaDTO> getActorPeliculas() {
		// TODO Auto-generated method stub
		return ActorPeliculaUtil.dtoToModel(actorPeliculaService.getActorPeliculas());
	}

	@Override
	@RequestMapping(value = "/actorPelicula/add", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Guardado actorPelicula", notes = "metodo encargado de salvar a un actorPelicula en la base de datos")
	public ActorPeliculaDTO saveActorPelicula(@RequestBody ActorPeliculaDTO dto) {
		// TODO Auto-generated method stub
		return ActorPeliculaUtil.objDtoToModel(actorPeliculaService.saveActorPelicula(ActorPeliculaUtil.objModelToDto(dto)));
	}

	@Override
	@RequestMapping(value = "/actorPelicula/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "actorPelicula por id", notes = "Trae un actorPelicula por id")
	public ActorPeliculaDTO getActorPeliculaById(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return ActorPeliculaUtil.objDtoToModel(actorPeliculaService.getActorPeliculaById(id));
	}

	@RequestMapping(value = "/actorPeliculas/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Listado de actorPeliculas por id", notes = "Trae una lista completa de actorPeliculas por id")
	@Override
	public List<ActorPeliculaDTO> getActorPeliculasById(@PathVariable Long id) {
		// TODO Auto-generated method stub
	    ArrayList<Long> miLista = new ArrayList<Long>();
		miLista.add(id);
		return ActorPeliculaUtil.dtoToModel(actorPeliculaService.getActorPeliculasById(miLista));
	}
	
}
