package es.vortech.peliculapp.controller.implement;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.vortech.peliculapp.controller.IPeliculaController;
import es.vortech.peliculapp.controller.dto.PeliculaDTO;
import es.vortech.peliculapp.service.IPeliculaService;
import es.vortech.peliculapp.util.PeliculaUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Servicio de Pelicula", description = "Esta api tiene las operaciones sobre la entidad pelicula")
public class PeliculaController implements IPeliculaController {

	@Autowired
	IPeliculaService peliculaService;

	
	@RequestMapping(value = "/pelicula", method = RequestMethod.GET, produces = "application/json")	
    @ApiOperation(value = "Listado de peliculas", notes = "Trae una lista completa de peliculas")
	@Override
	public List<PeliculaDTO> getPeliculas() {
		// TODO Auto-generated method stub
		return PeliculaUtil.dtoToModel(peliculaService.getPeliculas());
	}

	@Override
	@RequestMapping(value = "/pelicula/add", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Guardado pelicula", notes = "metodo encargado de salvar a un pelicula en la base de datos")
	public PeliculaDTO savePelicula(@RequestBody PeliculaDTO dto) {
		// TODO Auto-generated method stub
		return PeliculaUtil.objDtoToModel(peliculaService.savePelicula(PeliculaUtil.objModelToDto(dto)));
	}

	@Override
	@RequestMapping(value = "/pelicula/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "pelicula por id", notes = "Trae un pelicula por id")
	public PeliculaDTO getPeliculaById(@PathVariable Long id) {
		// TODO Auto-generated method stub
		return PeliculaUtil.objDtoToModel(peliculaService.getPeliculaById(id));
	}

	@RequestMapping(value = "/peliculas/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Listado de peliculas por id", notes = "Trae una lista completa de peliculas por id")
	@Override
	public List<PeliculaDTO> getPeliculasById(@PathVariable Long id) {
		// TODO Auto-generated method stub
	    ArrayList<Long> miLista = new ArrayList<Long>();
		miLista.add(id);
		return PeliculaUtil.dtoToModel(peliculaService.getPeliculaById(miLista));
	}
}
