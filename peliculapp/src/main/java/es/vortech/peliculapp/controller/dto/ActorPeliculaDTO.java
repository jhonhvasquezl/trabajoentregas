package es.vortech.peliculapp.controller.dto;

public class ActorPeliculaDTO {
	private Long id;
	private ActorDTO actorIdactor;
	private PeliculaDTO peliculaIdpelicula;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ActorDTO getActorIdactor() {
		return actorIdactor;
	}
	public void setActorIdactor(ActorDTO actorIdactor) {
		this.actorIdactor = actorIdactor;
	}
	public PeliculaDTO getPeliculaIdpelicula() {
		return peliculaIdpelicula;
	}
	public void setPeliculaIdpelicula(PeliculaDTO peliculaIdpelicula) {
		this.peliculaIdpelicula = peliculaIdpelicula;
	}
	
	
}
