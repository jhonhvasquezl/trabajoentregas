package es.vortech.peliculapp.controller;

import java.util.List;

import es.vortech.peliculapp.controller.dto.ActorDTO;

public interface IActorController {
	
	List<ActorDTO> getActors();
	ActorDTO saveActor(ActorDTO dto);
	ActorDTO getActorById(Long id);
	List<ActorDTO> getActorsById(Long id);
}
