package es.vortech.peliculapp.controller;

import java.util.List;

import es.vortech.peliculapp.controller.dto.PeliculaDTO;


public interface IPeliculaController {

	List<PeliculaDTO> getPeliculas();
	PeliculaDTO savePelicula(PeliculaDTO dto);
	PeliculaDTO getPeliculaById(Long id);
	List<PeliculaDTO> getPeliculasById(Long id);
}
