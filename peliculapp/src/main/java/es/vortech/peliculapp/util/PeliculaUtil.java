package es.vortech.peliculapp.util;

import java.util.ArrayList;
import java.util.List;

import es.vortech.peliculapp.controller.dto.PeliculaDTO;
import es.vortech.peliculapp.repository.model.Pelicula;



public class PeliculaUtil {
	
	public static List<Pelicula> modelToDto(List<PeliculaDTO> peliculaDto)
	{
		List<Pelicula> listaPelicula =new ArrayList<>(); 
		for (PeliculaDTO dto:peliculaDto){
			Pelicula pelicula = new Pelicula();
			pelicula.setId(dto.getId());
			pelicula.setTitulo(dto.getTitulo());
			pelicula.setGenero(dto.getGenero());
			pelicula.setNosscar(dto.getNosscar());
			listaPelicula.add(pelicula);
		}
		return listaPelicula;
	}
	
	public static List<PeliculaDTO> dtoToModel(List<Pelicula> pelicula)
	{
		List<PeliculaDTO> listaPelicula =new ArrayList<>(); 
		for (Pelicula dto:pelicula){
			PeliculaDTO peliculaDTO = new PeliculaDTO();
			peliculaDTO.setId(dto.getId());
			peliculaDTO.setTitulo(dto.getTitulo());
			peliculaDTO.setGenero(dto.getGenero());
			peliculaDTO.setNosscar(dto.getNosscar());
			listaPelicula.add(peliculaDTO);
		}
		return listaPelicula;
	}
	
	public static PeliculaDTO objDtoToModel(Pelicula dto){
		PeliculaDTO peliculaDTO = new PeliculaDTO();
		peliculaDTO.setId(dto.getId());
		peliculaDTO.setTitulo(dto.getTitulo());
		peliculaDTO.setGenero(dto.getGenero());
		peliculaDTO.setNosscar(dto.getNosscar());
		return peliculaDTO;
	}
	
	public static Pelicula objModelToDto(PeliculaDTO dto){
		Pelicula pelicula = new Pelicula();

		pelicula.setId(dto.getId());
		pelicula.setTitulo(dto.getTitulo());
		pelicula.setGenero(dto.getGenero());
		pelicula.setNosscar(dto.getNosscar());
		return pelicula;
	}
}
