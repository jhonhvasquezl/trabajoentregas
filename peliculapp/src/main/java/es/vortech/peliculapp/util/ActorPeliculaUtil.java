package es.vortech.peliculapp.util;

import java.util.ArrayList;
import java.util.List;

import es.vortech.peliculapp.controller.dto.ActorPeliculaDTO;
import es.vortech.peliculapp.repository.model.ActorPelicula;



public class ActorPeliculaUtil {

	public static List<ActorPelicula> modelToDto(List<ActorPeliculaDTO> actorPeliculaDto)
	{
		List<ActorPelicula> listaActorPelicula =new ArrayList<>(); 
		for (ActorPeliculaDTO dto:actorPeliculaDto){
			ActorPelicula actorPelicula = new ActorPelicula();
			actorPelicula.setId(dto.getId());
			actorPelicula.setActorIdactor(ActorUtil.objModelToDto(dto.getActorIdactor()));
			actorPelicula.setPeliculaIdpelicula(PeliculaUtil.objModelToDto(dto.getPeliculaIdpelicula()));
			listaActorPelicula.add(actorPelicula);
		}
		return listaActorPelicula;
	}
	public static List<ActorPeliculaDTO> dtoToModel(List<ActorPelicula> actorPelicula)
	{
		List<ActorPeliculaDTO> listaActorPelicula =new ArrayList<>(); 
		for (ActorPelicula dto:actorPelicula){
			ActorPeliculaDTO actorPeliculaDTO = new ActorPeliculaDTO();
			actorPeliculaDTO.setId(dto.getId());
			actorPeliculaDTO.setActorIdactor(ActorUtil.objDtoToModel(dto.getActorIdactor()));
			actorPeliculaDTO.setPeliculaIdpelicula(PeliculaUtil.objDtoToModel(dto.getPeliculaIdpelicula()));
			listaActorPelicula.add(actorPeliculaDTO);
		}
		return listaActorPelicula;
	}
	public static ActorPeliculaDTO objDtoToModel(ActorPelicula actorPelicula){
		ActorPeliculaDTO actorPeliculaDTO = new ActorPeliculaDTO();
		actorPeliculaDTO.setId(actorPelicula.getId());
		actorPeliculaDTO.setActorIdactor(ActorUtil.objDtoToModel(actorPelicula.getActorIdactor()));
		actorPeliculaDTO.setPeliculaIdpelicula(PeliculaUtil.objDtoToModel(actorPelicula.getPeliculaIdpelicula()));
		return actorPeliculaDTO;
	}
	
	public static ActorPelicula objModelToDto(ActorPeliculaDTO dto){
		ActorPelicula actorPelicula = new ActorPelicula();
		actorPelicula.setId(dto.getId());
		actorPelicula.setActorIdactor(ActorUtil.objModelToDto(dto.getActorIdactor()));
		actorPelicula.setPeliculaIdpelicula(PeliculaUtil.objModelToDto(dto.getPeliculaIdpelicula()));
		return actorPelicula;
	}
}
