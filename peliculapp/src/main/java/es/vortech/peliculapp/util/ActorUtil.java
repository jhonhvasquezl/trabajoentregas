package es.vortech.peliculapp.util;

import java.util.ArrayList;
import java.util.List;

import es.vortech.peliculapp.controller.dto.ActorDTO;
import es.vortech.peliculapp.repository.model.Actor;

public class ActorUtil {
	
	public static List<Actor> modelToDto(List<ActorDTO> actorDto)
	{
		List<Actor> listaActor =new ArrayList<>(); 
		for (ActorDTO dto:actorDto){
			Actor actor = new Actor();
			actor.setId(dto.getId());
			actor.setNombre(dto.getNombre());
			listaActor.add(actor);
		}
		return listaActor;
	}
	public static List<ActorDTO> dtoToModel(List<Actor> actor)
	{
		List<ActorDTO> listaActor =new ArrayList<>(); 
		for (Actor dto:actor){
			ActorDTO actorDTO = new ActorDTO();
			actorDTO.setId(dto.getId());
			actorDTO.setNombre(dto.getNombre());
			listaActor.add(actorDTO);
		}
		return listaActor;
	}
	public static ActorDTO objDtoToModel(Actor actor){
		ActorDTO actorDTO = new ActorDTO();
		actorDTO.setId(actor.getId());
		actorDTO.setNombre(actor.getNombre());
		return actorDTO;
	}
	
	public static Actor objModelToDto(ActorDTO actorDTO){
		Actor actor = new Actor();
		actor.setId(actorDTO.getId());
		actor.setNombre(actorDTO.getNombre());
		return actor;
	}
}
