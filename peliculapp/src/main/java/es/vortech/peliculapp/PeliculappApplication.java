package es.vortech.peliculapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeliculappApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeliculappApplication.class, args);
	}

}
