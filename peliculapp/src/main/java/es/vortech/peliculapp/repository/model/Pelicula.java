package es.vortech.peliculapp.repository.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pelicula")	
public class Pelicula {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idpelicula")
	private Long id;
	@Column(name = "titulo")
	private String titulo;
	@Column(name = "genero")
	private String genero;
	@Column(name = "nosscar")
	private int nosscar;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getNosscar() {
		return nosscar;
	}

	public void setNosscar(int nosscar) {
		this.nosscar = nosscar;
	}

	
}
