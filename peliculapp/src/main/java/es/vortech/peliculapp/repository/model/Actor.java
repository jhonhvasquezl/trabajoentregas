package es.vortech.peliculapp.repository.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "actor")	
public class Actor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="idactor")	
	private Long id;
	@Column(name="nombre")	
	private String nombre;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
