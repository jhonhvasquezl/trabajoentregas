package es.vortech.peliculapp.repository.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "actor_pelicula")	
public class ActorPelicula {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name="idactor_pelicula")	
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actor_idactor", referencedColumnName = "idactor")
	private Actor actorIdactor;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pelicula_idpelicula", referencedColumnName = "idpelicula")
	private Pelicula peliculaIdpelicula;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Actor getActorIdactor() {
		return actorIdactor;
	}
	public void setActorIdactor(Actor actorIdactor) {
		this.actorIdactor = actorIdactor;
	}
	public Pelicula getPeliculaIdpelicula() {
		return peliculaIdpelicula;
	}
	public void setPeliculaIdpelicula(Pelicula peliculaIdpelicula) {
		this.peliculaIdpelicula = peliculaIdpelicula;
	}
	
	
}
