package es.vortech.peliculapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.vortech.peliculapp.repository.model.Actor;	

public interface IActorRepository extends JpaRepository<Actor, Long> {	
}
