package es.vortech.peliculapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import es.vortech.peliculapp.repository.model.ActorPelicula;

public interface IActorPeliculaRepository extends JpaRepository<ActorPelicula, Long> {	
}