package es.vortech.peliculapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.vortech.peliculapp.repository.model.Pelicula;

public interface IPeliculaRepository extends JpaRepository<Pelicula, Long> {	
}