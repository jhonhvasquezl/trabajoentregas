package es.vortech.peliculapp.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.vortech.peliculapp.repository.IPeliculaRepository;
import es.vortech.peliculapp.repository.model.Pelicula;
import es.vortech.peliculapp.service.IPeliculaService;

@Service	
public class PeliculaService implements IPeliculaService {

	@Autowired
	IPeliculaRepository pr;

	@Override
	public List<Pelicula> getPeliculas() {
		// TODO Auto-generated method stub
		return pr.findAll();
	}

	@Override
	public Pelicula savePelicula(Pelicula pelicula) {
		// TODO Auto-generated method stub
		return pr.save(pelicula);
	}

	@Override
	public Pelicula getPeliculaById(Long id) {
		// TODO Auto-generated method stub
		return pr.getOne(id);
	}

	@Override
	public List<Pelicula> getPeliculaById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return pr.findAllById(ids);
	}
	
	

}
