package es.vortech.peliculapp.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.vortech.peliculapp.repository.IActorPeliculaRepository;
import es.vortech.peliculapp.repository.model.ActorPelicula;
import es.vortech.peliculapp.service.IActorPeliculaService;

@Service	
public class ActorPeliculaService implements IActorPeliculaService {

	@Autowired
	IActorPeliculaRepository ar;
	
	@Override
	public List<ActorPelicula> getActorPeliculas() {
		// TODO Auto-generated method stub
		return ar.findAll();
	}

	@Override
	public ActorPelicula saveActorPelicula(ActorPelicula actorPelicula) {
		// TODO Auto-generated method stub
		return ar.save(actorPelicula);
	}

	@Override
	public ActorPelicula getActorPeliculaById(Long id) {
		// TODO Auto-generated method stub
		return ar.getOne(id);
	}

	@Override
	public List<ActorPelicula> getActorPeliculasById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return ar.findAllById(ids);
	}

}
