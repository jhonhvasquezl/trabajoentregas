package es.vortech.peliculapp.service.implement;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.vortech.peliculapp.repository.IActorRepository;
import es.vortech.peliculapp.repository.model.Actor;
import es.vortech.peliculapp.service.IActorService;

@Service	
public class ActorService implements IActorService {

	@Autowired
	IActorRepository ar;

	@Override
	public Actor saveActor(Actor actor) {
		// TODO Auto-generated method stub
		return ar.save(actor);
	}
	@Override
	public List<Actor> getActors() {
		// TODO Auto-generated method stub
		return ar.findAll();
	}
	@Override
	public Actor getActorById(Long id) {
		// TODO Auto-generated method stub
		return ar.getOne(id);
	}
	@Override
	public List<Actor> getActorsById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		
		return ar.findAllById(ids);
	}

}
