package es.vortech.peliculapp.service;

import java.util.List;

import es.vortech.peliculapp.repository.model.ActorPelicula;


public interface IActorPeliculaService {

	public List<ActorPelicula> getActorPeliculas();
	public ActorPelicula saveActorPelicula(ActorPelicula actorPelicula);
	public ActorPelicula getActorPeliculaById(Long id);
	public List<ActorPelicula> getActorPeliculasById(Iterable<Long> ids);
}
