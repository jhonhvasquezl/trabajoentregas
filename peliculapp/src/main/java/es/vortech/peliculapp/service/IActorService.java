package es.vortech.peliculapp.service;

import java.util.List;

import es.vortech.peliculapp.repository.model.Actor;

public interface IActorService {

	public List<Actor> getActors();
	public Actor saveActor(Actor actor);
	public Actor getActorById(Long id);
	public List<Actor> getActorsById(Iterable<Long> ids);

}
