package es.vortech.peliculapp.service;

import java.util.List;

import es.vortech.peliculapp.repository.model.Pelicula;


public interface IPeliculaService {

	public List<Pelicula> getPeliculas();
	public Pelicula savePelicula(Pelicula pelicula);
	public Pelicula getPeliculaById(Long id);
	public List<Pelicula> getPeliculaById(Iterable<Long> ids);
}
