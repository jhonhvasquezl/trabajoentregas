-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2020 at 07:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `peliculapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `actor`
--

CREATE TABLE `actor` (
  `idactor` int(11) NOT NULL,
  `nombre` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actor`
--

INSERT INTO `actor` (`idactor`, `nombre`) VALUES
(1, 'Leonardo dicaprio'),
(2, 'Jhonny deep'),
(3, 'Al pacino'),
(4, 'Jennifer Lopez'),
(5, 'Sharon Stone'),
(6, 'Dakota Fanning');

-- --------------------------------------------------------

--
-- Table structure for table `actor_pelicula`
--

CREATE TABLE `actor_pelicula` (
  `idactor_pelicula` int(11) NOT NULL,
  `actor_idactor` int(11) NOT NULL,
  `pelicula_idpelicula` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actor_pelicula`
--

INSERT INTO `actor_pelicula` (`idactor_pelicula`, `actor_idactor`, `pelicula_idpelicula`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 4, 1),
(4, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pelicula`
--

CREATE TABLE `pelicula` (
  `idpelicula` int(11) NOT NULL,
  `titulo` varchar(256) DEFAULT NULL,
  `genero` varchar(256) DEFAULT NULL,
  `nosscar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pelicula`
--

INSERT INTO `pelicula` (`idpelicula`, `titulo`, `genero`, `nosscar`) VALUES
(1, 'Frozen', 'Infantil', 2),
(2, 'Big Fish', 'Drama', 3),
(3, 'Efecto mariposa', 'Drama', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actor`
--
ALTER TABLE `actor`
  ADD PRIMARY KEY (`idactor`);

--
-- Indexes for table `actor_pelicula`
--
ALTER TABLE `actor_pelicula`
  ADD PRIMARY KEY (`idactor_pelicula`),
  ADD KEY `fk_actor_pelicula_actor_idx` (`actor_idactor`),
  ADD KEY `fk_actor_pelicula_pelicula1_idx` (`pelicula_idpelicula`);

--
-- Indexes for table `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`idpelicula`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actor`
--
ALTER TABLE `actor`
  MODIFY `idactor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `actor_pelicula`
--
ALTER TABLE `actor_pelicula`
  MODIFY `idactor_pelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `idpelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actor_pelicula`
--
ALTER TABLE `actor_pelicula`
  ADD CONSTRAINT `fk_actor_pelicula_actor` FOREIGN KEY (`actor_idactor`) REFERENCES `actor` (`idactor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_actor_pelicula_pelicula1` FOREIGN KEY (`pelicula_idpelicula`) REFERENCES `pelicula` (`idpelicula`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
